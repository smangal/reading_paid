package com.mathfriendzy.notification;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mathfriendzy.controller.multifriendzy.MultiFriendzyRound;
import com.mathfriendzy.controller.multifriendzy.MultiFriendzyWinnerScreen;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.model.language.translation.Translation;
import com.readingfriendzypaid.R;

/**
 * This activity will open dialog for notification when user on the math friendzy application
 * @author Yashwant Singh
 *
 */
public class PopUpActivityForNotification extends AdBaseActivity implements OnClickListener
{
	private Button btnOk 		= null;
	private Button btnCencel 	= null;
	private TextView txtMessage = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pop_up_activity_for_notification);
		
		this.setWidgetsReferences();
		this.setListenerOnWidgets();
		this.setTextFromtranselation();
	}
	
	/**
	 * This method set widgets references
	 */
	private void setWidgetsReferences()
	{
		btnOk 		= (Button) findViewById(R.id.btnOk);
		btnCencel 	= (Button) findViewById(R.id.btnCencel);
		txtMessage  = (TextView) findViewById(R.id.txtMessageFromNotification);
		
		txtMessage.setText(ProcessNotification.sentNotificationData.getMessage());
	}
	
	/**
	 * This method set text from translation
	 */
	private void setTextFromtranselation()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
		transeletion.closeConnection();
	}
	
	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		btnOk.setOnClickListener(this);
		btnCencel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{		
		case R.id.btnOk : 
			finish();
			this.clickOnOkButton();
			break;
		case R.id.btnCencel : 
			ProcessNotification.isFromNotification = false;
			finish();
			break;
		}
	}
	
	/**
	 * This method call when click on ok button
	 */
	private void clickOnOkButton()
	{
		if(ProcessNotification.sentNotificationData.getWinnerId().equals("-1"))
		{
			Intent intent = new Intent(this,MultiFriendzyRound.class);
			startActivity(intent);
		}
		else
		{
			Intent intent = new Intent(this,MultiFriendzyWinnerScreen.class);
			startActivity(intent);
		}
	}
}
