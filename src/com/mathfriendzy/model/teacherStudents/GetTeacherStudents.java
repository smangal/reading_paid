package com.mathfriendzy.model.teacherStudents;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.model.registration.UserPlayerDto;

/**
 * This Get Teacher Student From the server
 * @author Yashwant Singh
 *
 */
public class GetTeacherStudents 
{	
	private Context context = null;
	
	/**
	 *Constructor 
	 * @param context
	 */
	public GetTeacherStudents(Context context)
	{
		this.context = context;
	}
	
	/**
	 * This method return the students from the server
	 * @param userId
	 * @param offset
	 * @return
	 */
	public ArrayList<UserPlayerDto> getStudents(String userId , int offset)
	{
		String action = "getStudents";
		
		String strURL = COMPLETE_URL + "action=" + action + "&"
									 + "userId=" + userId + "&"
									 + "offset=" + offset + "&appId="+CommonUtils.APP_ID;
		//Log.e("USRL", strURL);
		return this.parseJson(CommonUtils.readFromURL(strURL));
		
	}
	
	/**
	 * This method parse json String for student data
	 * @param jsonString
	 * @return
	 */
	private ArrayList<UserPlayerDto> parseJson(String jsonString)
	{
		ArrayList<UserPlayerDto> userPlayer = new ArrayList<UserPlayerDto>();
		
		//Log.e("Json", jsonString);
		
		try
		{
			JSONObject jObject = new JSONObject(jsonString);
			try
			{
				JSONArray jsonArray = jObject.getJSONArray("data");
				
				for( int i = 0 ; i < jsonArray.length() ; i ++ )
				{
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					
					UserPlayerDto userPlayerDto = new UserPlayerDto();
					userPlayerDto.setFirstname(jsonObject.getString("fName"));
					userPlayerDto.setLastname(jsonObject.getString("lName"));
					userPlayerDto.setSchoolId(jsonObject.getString("schoolId"));
					userPlayerDto.setSchoolName(jsonObject.getString("schoolName"));
					userPlayerDto.setGrade(jsonObject.getString("grade"));
					userPlayerDto.setTeacherUserId(jsonObject.getString("teacherUserId"));
					userPlayerDto.setTeacherFirstName(jsonObject.getString("teacherFirstName"));
					userPlayerDto.setTeacheLastName(jsonObject.getString("teacherLastName"));
					userPlayerDto.setIndexOfAppearance(jsonObject.getString("indexOfAppearance"));
					userPlayerDto.setParentUserId(jsonObject.getString("parentUserId"));
					userPlayerDto.setPlayerid(jsonObject.getString("playerId"));
					userPlayerDto.setCompletelavel(jsonObject.getString("competeLevel"));
					userPlayerDto.setImageName(jsonObject.getString("profileImageId"));
					userPlayerDto.setCoin(jsonObject.getString("coins"));
					userPlayerDto.setPoints(jsonObject.getString("points"));
					userPlayerDto.setCity(jsonObject.getString("city"));
					userPlayerDto.setStateName(jsonObject.getString("state"));
					userPlayerDto.setUsername(jsonObject.getString("userName"));
					userPlayer.add(userPlayerDto);
				}
			}
			catch(JSONException e1)
			{
				return userPlayer;
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return userPlayer;
	}
}
