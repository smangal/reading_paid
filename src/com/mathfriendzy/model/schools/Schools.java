package com.mathfriendzy.model.schools;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;

public class Schools 
{
	private ArrayList<SchoolDTO> schoolNameList = null;
	
	/**
	 * This method return school list from server url
	 * @param countryIso
	 * @param stateCodeName
	 * @param city
	 * @param zip
	 * @return
	 */
	public ArrayList<SchoolDTO> getSchoolList(String countryIso , String stateCodeName , String city , String zip)
	{
		String action = "getSchoolsForCityStateCountry";
		String encodedCity = "";
		
		try 
		{
			encodedCity = URLEncoder.encode(city, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			
		}
		
		String strUrl = COMPLETE_URL  + "action=" 	+ 	action 			+ "&" 
				+ "countryIso=" 	+ 	countryIso.toString()	+ "&"
				+ "stateCode="		+	stateCodeName		+ "&"
				+ "city="		+	encodedCity		+ "&"
				+ "zip="		+	zip;
		
		schoolNameList = this.parseSchoolJson(CommonUtils.readFromURL(strUrl));
		
	 return schoolNameList;
	}
	
	/**
	 * Parse school json data from url
	 * @param jsonString
	 * @return
	 */
	private ArrayList<SchoolDTO> parseSchoolJson(String jsonString)
	{
		//Log.e("Schools", "Json String " + jsonString);
		ArrayList<SchoolDTO> schoolNameList = new ArrayList<SchoolDTO>();
		SchoolDTO schoolObj = null;
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jArray = jObject.getJSONArray("data");
			
			for(int i = 0; i < jArray.length() ; i++)
			{	
				schoolObj = new SchoolDTO();
				JSONObject jObject2 = jArray.getJSONObject(i);
				schoolObj.setSchoolId(jObject2.getString("schoolId"));
				schoolObj.setSchoolName(jObject2.getString("name"));						
				if(!MathFriendzyHelper.checkForEmptyFields(schoolObj
						.getSchoolName().trim()))
					schoolNameList.add(schoolObj);
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return schoolNameList;
	}
	
	/**
	 * Get school list from server url other than Canada and US
	 * @param countryIso
	 * @return
	 */
	public ArrayList<SchoolDTO> getSchoolListOtherThanUSandCanada(String countryIso)
	{
		schoolNameList = new ArrayList<SchoolDTO>();
		String action = "schools";
		String strUrl = COMPLETE_URL  + "action=" 	+ 	action 	+ "&" + "countryIso=" + countryIso.toString();
		schoolNameList = this.parseSchoolJson(CommonUtils.readFromURL(strUrl));
		return schoolNameList;
	}
	
	/**
	 * Get teacher list corresponding to the given school id
	 * @param schoolId
	 * @return
	 */
	public ArrayList<TeacherDTO> getTeacherList(String schoolId)
	{
		ArrayList<TeacherDTO> techerList = new ArrayList<TeacherDTO>();
		String strUrl = COMPLETE_URL + "action=" + "getTeachers&schoolId=" + schoolId;
			techerList = this.parseTeacheJson(CommonUtils.readFromURL(strUrl));
		return techerList;
	}
	
	/**
	 * Parse school json from server url data
	 * @param jsonString
	 * @return
	 */
   private ArrayList<TeacherDTO> parseTeacheJson(String jsonString)
   {	   
	    ArrayList<TeacherDTO> techerList = new ArrayList<TeacherDTO>();
	    TeacherDTO techerObj = null;
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jArray = jObject.getJSONArray("data");
			
			for(int i = 0; i < jArray.length() ; i++)
			{	
				techerObj = new TeacherDTO();
				JSONObject jObject2 = jArray.getJSONObject(i);
				techerObj.setfName(jObject2.getString("fname"));
				techerObj.setlName(jObject2.getString("lname"));
				techerObj.setTeacherUserId(jObject2.getString("userId"));
				
				techerList.add(techerObj);
			}
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		return techerList;
   }
  
   /**
    * This method register school on server
    * @param schoolName
    * @param countryId
    * @param countryIso
    * @param city
    * @return
    */
   public String registereSchool(String schoolName,String countryId,String countryIso,String city)
   {
	   String action = "addSchool";
	   
	   String encodeschooName   = null;
	   String encodedCity = "";
		
		try 
		{
			encodeschooName = URLEncoder.encode(schoolName, "UTF-8");
			encodedCity = URLEncoder.encode(city, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			
		}
		
	   String strUrl = COMPLETE_URL  + "action=" 	+ 	action	+ "&" 
				+ "name=" 	+ 	encodeschooName	+ "&"
				+ "city="		+	encodedCity		+ "&"
				+ "state="		+ "&"
				+ "country="	+	countryIso      +"&"
				+ "stateId="    + "&"
				+ "countryId="  + countryId + "&"
				+ "zip=";
	   
	    return this.parseRegisteredSchool(CommonUtils.readFromURL(strUrl));
   }
   
   /**
    * This method parse json String after registration of school
    * @param jsonString
    * @return
    */
   private String parseRegisteredSchool(String jsonString)
   {
	   String userResult = "";
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			userResult = jObject.getString("code");
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		return userResult;
   }
}
