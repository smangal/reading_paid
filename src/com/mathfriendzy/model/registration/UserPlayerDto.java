package com.mathfriendzy.model.registration;

public class UserPlayerDto 
{
	private  String parentUserId ;
	private  String playerid;
	private  String firstname;
	private  String lastname;
	private  String username;
	private  String city;
	private  String coin;
	private  String completelavel;
	private  String grade;
	private  String points;
	protected byte[] profileImage;
	private String imageName;
	
	private  String schoolId;
	private  String schoolName;
	private  String teacherFirstName = "";
	private  String teacheLastName;
	private  String teacherUserId;
	private  String stateName;
	private  String indexOfAppearance;
	private String studentIdByTeacher = "";
	
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}
	public String getPlayerid() {
		return playerid;
	}
	public void setPlayerid(String playerid) {
		this.playerid = playerid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCoin() {
		return coin;
	}
	public void setCoin(String coin) {
		this.coin = coin;
	}
	public String getCompletelavel() {
		return completelavel;
	}
	public void setCompletelavel(String completelavel) {
		this.completelavel = completelavel;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public byte [] getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(byte [] profileImage) {
		this.profileImage = profileImage;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacheLastName() {
		return teacheLastName;
	}
	public void setTeacheLastName(String teacheLastName) {
		this.teacheLastName = teacheLastName;
	}
	public String getTeacherUserId() {
		return teacherUserId;
	}
	public void setTeacherUserId(String teacherUserId) {
		this.teacherUserId = teacherUserId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getIndexOfAppearance() {
		return indexOfAppearance;
	}
	public void setIndexOfAppearance(String indexOfAppearance) {
		this.indexOfAppearance = indexOfAppearance;
	}
	
	public String getStudentIdByTeacher() {		
		return studentIdByTeacher;
	}
	
	public void setStudentIdByTeacher(String studentIdByTeacher){
		this.studentIdByTeacher = studentIdByTeacher;
	}
	
}
