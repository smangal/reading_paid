package com.mathfriendzy.customview;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.result.JsonAsyncTaskForScore;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.readingfriendzypaid.R;

/**
 * This class Creates the dynamic layout
 * @author Yashwant Singh
 *
 */
public class DynamicLayout 
{

	ArrayList<Button>   btnResultList = null;
	ArrayList<Button>   btnEditList   = null;
	ArrayList<ImageView> imgChekedList= null;

	private ImageView imgCheck = null;
	private boolean isChecked  = false;
	private ImageView imgCheckHoderImage =  null;

	private Context context = null;
	private boolean isTab	= false;

	private SharedPreferences sheredPreference = null;

	public DynamicLayout(Context context)
	{
		Log.e("DynamiCLayout", "inside constructor");
		this.context = context;
		isTab = context.getResources().getBoolean(R.bool.isTabSmall);

		sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);

		btnResultList = new ArrayList<Button>();
		btnEditList   = new ArrayList<Button>();
		imgChekedList = new ArrayList<ImageView>();
	}

	/**
	 * Create dynamic layout for displaying user player list
	 */
	public void createDyanamicLayoutForDisplayUserPlayer(final ArrayList<UserPlayerDto> userPlayerList , LinearLayout userPlayer, boolean isBlack) 
	{
		int color = 0;
		if(isBlack)
			color = Color.BLACK;
		else
			color = Color.WHITE;
		/*LinearLayout userPlayer = (LinearLayout) findViewById(R.id.userPlayerLayout);*/

		for(int i = 0 ; i < userPlayerList.size() ; i++)
		{
			RelativeLayout.LayoutParams paramsImgCheck = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			if(isTab)
			{
				paramsImgCheck = new RelativeLayout.LayoutParams(70, 70);
			}
			paramsImgCheck.addRule(RelativeLayout.ALIGN_LEFT);
			paramsImgCheck.addRule(RelativeLayout.CENTER_VERTICAL);

			RelativeLayout layout = new RelativeLayout(context);

			imgCheck = new ImageView(context);
			imgCheck.setId(1);

			if(sheredPreference.getString(PLAYER_ID, "").equals(userPlayerList.get(i).getPlayerid()))
			{				
				imgCheck.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
				imgCheckHoderImage = imgCheck;
			}
			else
			{
				imgCheck.setBackgroundResource(R.drawable.mf_check_box_ipad);
			}

			imgChekedList.add(imgCheck);
			layout.addView(imgCheck,paramsImgCheck);

			imgCheck.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{	
					/*if(userPlayerList.size() == 1 && sheredPreference.getString(PLAYER_ID, "").equals(""))//if one player exist then no action perform)
					{

					}*/
					/*if(userPlayerList.size() > 1 && sheredPreference.getString(PLAYER_ID, "").equals(""))//if one player exist then no action perform)
					{*/
					SharedPreferences.Editor editor = sheredPreference.edit();

					for( int  i = 0 ; i < imgChekedList.size() ; i ++ )
					{
						if(v == imgChekedList.get(i))
						{

							if(imgCheckHoderImage != null)
								imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);

							if(!isChecked)
							{															
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
								editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
								editor.putString("userId", userPlayerList.get(i).getParentUserId());
								//editor.putString("challengerId", "-1");
								MainActivity.IS_CALL_FROM_LEARNING_CENTER = 0;
								imgCheckHoderImage = imgChekedList.get(i);
								isChecked = !isChecked;
							}
							else
							{

								if(imgCheckHoderImage == imgChekedList.get(i))
								{
									//Log.e(TAG, "inside if");
									imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
									editor.clear();
									MainActivity.IS_CALL_FROM_LEARNING_CENTER = 1;
									isChecked = !isChecked;
								}
								else
								{
									imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);
									imgCheckHoderImage = imgChekedList.get(i);
									imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
									editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
									editor.putString("userId", userPlayerList.get(i).getParentUserId());
									MainActivity.IS_CALL_FROM_LEARNING_CENTER = 0;
								}
							}
							editor.commit();
						}
					}
					//}
				}
			});

			RelativeLayout.LayoutParams paramsPlayerName = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsPlayerName.addRule(RelativeLayout.RIGHT_OF,imgCheck.getId());
			TextView txtPlayerName = new TextView(context);
			txtPlayerName.setId(2);
			txtPlayerName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");
			txtPlayerName.setTextColor(color);
			if(isTab)
			{
				paramsPlayerName.setMargins(5, 15, 0, 0);
				txtPlayerName.setTextSize(20);
			}
			else
			{
				paramsPlayerName.setMargins(2, 5, 0, 0);
			}

			layout.addView(txtPlayerName,paramsPlayerName);



			RelativeLayout.LayoutParams paramsBtnEdit = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsBtnEdit.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);

			Button btnEdit = new Button(context);
			btnEdit.setId(4);
			if(isTab)
			{
				btnEdit.setBackgroundResource(R.drawable.pencil_small_ipad);
			}
			else
			{
				btnEdit.setBackgroundResource(R.drawable.edit_player_small);
			}
			btnEditList.add(btnEdit);
			layout.addView(btnEdit,paramsBtnEdit);

			btnEdit.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context))
					{
						CommonUtils.showInternetDialog(context);			
					}
					else
					{
						for( int  j = 0 ; j < btnEditList.size() ; j ++ )
						{
							if(v == btnEditList.get(j))
							{
								Intent intent = new Intent(context,EditRegisteredUserPlayer.class);
								intent.putExtra("playerId", userPlayerList.get(j).getPlayerid());
								intent.putExtra("userId", userPlayerList.get(j).getParentUserId());
								intent.putExtra("imageName" , userPlayerList.get(j).getImageName());
								intent.putExtra("callingActivity", context.getClass().getSimpleName());
								context.startActivity(intent);
							}
						}
					}
				}
			});


			RelativeLayout.LayoutParams paramsBtnResult = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsBtnResult.addRule(RelativeLayout.LEFT_OF,btnEdit.getId());
			paramsBtnResult.setMargins(0, 0, 10, 0);

			Button btnResult = new Button(context);
			btnResult.setId(3);
			if(isTab)
			{
				btnResult.setBackgroundResource(R.drawable.result_small_ipad);
			}
			else
			{
				btnResult.setBackgroundResource(R.drawable.result_player_small);
			}

			btnResultList.add(btnResult);
			layout.addView(btnResult,paramsBtnResult);

			btnResult.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context))
					{
						CommonUtils.showInternetDialog(context);			
					}
					else
					{
						for( int  j = 0 ; j < btnResultList.size() ; j ++ )
						{
							if(v == btnResultList.get(j))
							{
								int playerid = Integer.parseInt(userPlayerList.get(j).getPlayerid());
								int userid  = Integer.parseInt(userPlayerList.get(j).getParentUserId());
								String playerName  = userPlayerList.get(j).getFirstname()+" "
										+userPlayerList.get(j).getLastname();
								
								clickOnResult(userid, playerid, playerName);
								
							}
						}
					}

				}

			});
			userPlayer.addView(layout);
		}
	}

	/**
	 * Create dynamic layout for displaying user player list for friendzy notification
	 */
	public void createDynamicLayoutForFriendzy(final ArrayList<UserPlayerDto> userPlayerList , LinearLayout userPlayer,
			String strViewChallenge) {

		int color = Color.WHITE;

		for(int i = 0 ; i < userPlayerList.size() ; i++)
		{			
			RelativeLayout layout = new RelativeLayout(context);			

			RelativeLayout.LayoutParams paramsPlayerName = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			TextView txtPlayerName = new TextView(context);
			txtPlayerName.setId(6);			
			txtPlayerName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");
			txtPlayerName.setTextColor(color);
			if(isTab)
			{
				paramsPlayerName.setMargins(5, 20, 0, 0);
				txtPlayerName.setTextSize(20);
			}
			else
			{
				paramsPlayerName.setMargins(2, 15, 0, 0);
			}

			layout.addView(txtPlayerName,paramsPlayerName);

			RelativeLayout.LayoutParams paramsbtnView = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			paramsbtnView.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
			paramsbtnView.setMargins(0, 10, 10, 10);
			Button btnView = new Button(context);
			btnView.setId(7);

			btnView.setTextColor(color);
			btnView.setText(strViewChallenge);
			if(isTab)
			{
				btnView.setTextSize(20);
				btnView.setBackgroundResource(R.drawable.gjs_play_again);
			}
			else
			{
				btnView.setBackgroundResource(R.drawable.green_1);
			}
			btnEditList.add(btnView);
			layout.addView(btnView,paramsbtnView);

			btnView.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					if(!CommonUtils.isInternetConnectionAvailable(context))
					{
						CommonUtils.showInternetDialog(context);			
					}
					else
					{
						for( int  j = 0 ; j < btnEditList.size() ; j ++ )
						{
							if(v == btnEditList.get(j))
							{
								SharedPreferences sharedPreffPlayerInfo	 = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
								SharedPreferences.Editor editor = sharedPreffPlayerInfo.edit();
								editor.putString("challengerId", "-1");
								editor.putString("playerId", userPlayerList.get(j).getPlayerid());
								editor.putString("userId", userPlayerList.get(j).getParentUserId());
								editor.commit();
								Intent intent = new Intent(context,StudentChallengeActivity.class);								
								context.startActivity(intent);
							}
						}
					}
				}
			});

			userPlayer.addView(layout);
		}
	}



	/**
	 * Create dynamiv layout for see answer activity
	 * @param playEquationList
	 * @param userAnswerList
	 * @param layout
	 *//*
	public void createDynamicLayoutForSeeAnswer(ArrayList<LearningCenterTransferObj> playEquationList , ArrayList<String> userAnswerList , RelativeLayout layout)
	{

		for(int i = 0 ; i < userAnswerList.size() ; i ++ )
		{
			RelativeLayout innerLayout = new RelativeLayout(context);

			RelativeLayout.LayoutParams txtSrParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			txtSrParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT , RelativeLayout.TRUE);
			TextView txtSrNo = new TextView(context);
			txtSrNo.setText(i + 1 + ".");
			txtSrNo.setId(1000 + i);
			txtSrNo.setLayoutParams(txtSrParam);
			innerLayout.addView(txtSrNo);

			RelativeLayout.LayoutParams txtProblemLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);

			txtProblemLayout.addRule(RelativeLayout.RIGHT_OF,txtSrNo.getId());
			TextView txtProblem = new TextView(context);
			txtProblem.setText(playEquationList.get(i).getNumber1Str() + " " 
					+ playEquationList.get(i).getOperator() + " "
					+ playEquationList.get(i).getNumber2Str() + " = " 
					+ playEquationList.get(i).getProductStr());
			txtProblem.setId(2000 + i);
			txtProblem.setLayoutParams(txtProblemLayout);
			innerLayout.addView(txtProblem);

			RelativeLayout.LayoutParams txtUserAnswerLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			txtUserAnswerLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT , RelativeLayout.TRUE);

			TextView txtUserAnswer = new TextView(context);
			txtUserAnswer.setText(userAnswerList.get(i));
			txtUserAnswer.setLayoutParams(txtUserAnswerLayout);
			innerLayout.addView(txtUserAnswer);

			layout.addView(innerLayout);
		}
	}*/





	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class AddMathScroreOnServer extends AsyncTask<Void, Void, Void>
	{
		private ArrayList<MathResultTransferObj> mathResultTransferObjList  = null;
		private Context context;
		private String playDate;
		private int userid;
		private int playerid;
		private String playerName;
		boolean b;
		ProgressDialog pd ;

		AddMathScroreOnServer(ArrayList<MathResultTransferObj> mathResultTransferObjList,
				Context context, String playDate, int userid, int playerid, String playerName, 
				boolean b)
				{
			this.mathResultTransferObjList = mathResultTransferObjList;
			this.context        = context;
			this.playDate       = playDate;
			this.userid         = userid;
			this.playerid       = playerid;
			this.playerName     = playerName;
			this.b              = b;

				}

		@Override
		protected void onPreExecute() 
		{  
			pd = CommonUtils.getProgressDialog(context);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(context);
			for(int i = 0 ; i < mathResultTransferObjList.size() ; i ++ ){
				MathResultTransferObj mathobj = mathResultTransferObjList.get(i);
				mathobj.setGameType("Play");
				register.savePlayerScoreOnServerForloginuser(mathobj);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false)
			.execute(null,null,null); 

			super.onPostExecute(result);
		}
	}

	/**
	 * Create dynamic layout for displaying user player list
	 */
	@SuppressLint("InflateParams")
	public void createDyanamicLayoutForDisplayUserPlayer(final ArrayList<UserPlayerDto>
	userPlayerList , LinearLayout userPlayer, boolean isBlack , int youare){

		final ArrayList<SelectPlayerViewHolde> viewHolderList = new ArrayList<SelectPlayerViewHolde>();
		for(int i = 0 ; i < userPlayerList.size() ; i ++ ){
			View view = LayoutInflater.from(context).inflate(R.layout.select_player_layout, null);
			SelectPlayerViewHolde vHolder = new SelectPlayerViewHolde();
			vHolder.studentLayout = (RelativeLayout) view.findViewById(R.id.studentLayout);
			vHolder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
			vHolder.imgEdit = (ImageView) view.findViewById(R.id.imgEdit);
			vHolder.imgResult = (ImageView) view.findViewById(R.id.imgResult);

			if(youare == 1){//1 for teacher
				vHolder.imgEdit.setVisibility(ImageView.GONE);
			}

			if(sheredPreference.getString(PLAYER_ID, "")
					.equals(userPlayerList.get(i).getPlayerid())){	
				vHolder.studentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
				vHolder.txtStudentName.setTextColor(context.getResources().getColor(R.color.WHITE));
			}else{
				vHolder.studentLayout.setBackgroundResource(R.drawable.white_button_new_registration);
				vHolder.txtStudentName.setTextColor(context.getResources().getColor(R.color.BLACK));
			}
			vHolder.txtStudentName.setText(userPlayerList.get(i).getFirstname() + " " 
					+ userPlayerList.get(i).getLastname().charAt(0)+".");

			vHolder.studentLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					for(int i = 0 ; i < viewHolderList.size() ; i ++){
						if(v == viewHolderList.get(i).studentLayout){
							viewHolderList.get(i).studentLayout.setBackgroundResource
							(R.drawable.blue_button_new_registration);
							viewHolderList.get(i).txtStudentName.setTextColor
							(context.getResources().getColor(R.color.WHITE));

							//set selected student
							SharedPreferences.Editor editor = sheredPreference.edit();
							editor.putString(PLAYER_ID,userPlayerList.get(i).getPlayerid());
							editor.putString("userId", userPlayerList.get(i).getParentUserId());
							editor.commit();
						}else{
							viewHolderList.get(i).studentLayout.setBackgroundResource
							(R.drawable.white_button_new_registration);
							viewHolderList.get(i).txtStudentName.setTextColor
							(context.getResources().getColor(R.color.BLACK));
						}
					}
				}
			});

			vHolder.imgEdit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog(transeletion
								.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}
					else{
						for( int  i = 0 ; i < viewHolderList.size() ; i ++ ){
							if(v == viewHolderList.get(i).imgEdit)	{
								Intent intent = new Intent(context,EditRegisteredUserPlayer.class);
								intent.putExtra("playerId", userPlayerList.get(i).getPlayerid());
								intent.putExtra("userId", userPlayerList.get(i).getParentUserId());
								intent.putExtra("imageName" , userPlayerList.get(i).getImageName());
								intent.putExtra("callingActivity", context.getClass()
										.getSimpleName());
								context.startActivity(intent);
							}
						}
					}
				}
			});

			vHolder.imgResult.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(!CommonUtils.isInternetConnectionAvailable(context)){
						DialogGenerator dg = new DialogGenerator(context);
						Translation transeletion = new Translation(context);
						transeletion.openConnection();
						dg.generateWarningDialog
						(transeletion.getTranselationTextByTextIdentifier
								("alertMsgYouAreNotConnectedToTheInternet"));
						transeletion.closeConnection();
					}else{
						for(int i = 0 ; i < viewHolderList.size() ; i ++){
							if(v == viewHolderList.get(i).imgResult){
								int playerid = Integer.parseInt(userPlayerList.get(i)
										.getPlayerid());
								int userid  = Integer.parseInt(userPlayerList.get(i)
										.getParentUserId());
								String playerName  = userPlayerList.get(i).getFirstname()+" "
										+userPlayerList.get(i).getLastname();
								String imageName = userPlayerList.get(i).getImageName();
								clickOnResult(userid, playerid, playerName );
							}
						}		
					}
				}
			});

			userPlayer.addView(view);
			viewHolderList.add(vHolder);
		}
	}

	private class SelectPlayerViewHolde{
		private RelativeLayout studentLayout;
		private TextView txtStudentName;
		private ImageView imgEdit;
		private ImageView imgResult;
	}

	

	private void clickOnResult(int userid, int playerid,
			String playerName) {
		Date date  = new Date();

		String zero1 = "";
		String zero2 = "";
		if(date.getMonth() < 9)
			zero1 = "0";
		if(date.getDate() <= 9)
			zero2 = "0";
		String playDate  = (date.getYear()+1900) + "-"+zero1 + (date.getMonth() + 1)
				+ "-"+ zero2 +date.getDate();
		new JsonAsyncTaskForScore(context, playDate, userid, playerid, playerName, false)
		.execute(null,null,null);
		
	}
}
