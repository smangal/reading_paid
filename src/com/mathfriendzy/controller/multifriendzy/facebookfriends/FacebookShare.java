package com.mathfriendzy.controller.multifriendzy.facebookfriends;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.readingfriendzypaid.R;

public class FacebookShare extends AdBaseActivity implements OnClickListener
{
	private TextView txtTitleTopbar			= null;
	private TextView txtMessage				= null;
	private Button btnCancel				= null;
	private Button btnSubmit				= null;
	private EditText edtMsg					= null;

	private String message					= null;
	private String alertMsg					= null;
	private ProgressDialog pd;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facebook_share);

		message = CommonUtils.getFacebookShareMsg(this);

		getWidgetId();
		setWidgetText();
	}



	private void setWidgetText() 
	{
		Translation translate = new Translation(this);
		String text;
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier("btnTitlePostOnWall");
		txtTitleTopbar.setText(text);

		text = translate.getTranselationTextByTextIdentifier("btnTitleCancel");
		btnCancel.setText(text);

		text = translate.getTranselationTextByTextIdentifier("btnTitleSubmit");
		btnSubmit.setText(text);

		text = translate.getTranselationTextByTextIdentifier("txtMsgPlaceholderSaySomething");
		edtMsg.setHint(text);

		alertMsg = translate.getTranselationTextByTextIdentifier("alertMsgYourWallPostCompleted");

		translate.closeConnection();
		txtMessage.setText(message+"");

	}

	private void getWidgetId()
	{
		txtMessage		= (TextView) findViewById(R.id.txtMessage);
		txtTitleTopbar	= (TextView) findViewById(R.id.txtTitleTopbar);
		btnCancel		= (Button) findViewById(R.id.btncancel);
		btnSubmit		= (Button) findViewById(R.id.btnSubmit);
		edtMsg			= (EditText) findViewById(R.id.edtMsg);

		btnCancel.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);

	}//END getWidgetId method



	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btncancel:
			finish();
			break;

		case R.id.btnSubmit:
			message = edtMsg.getText().toString()+ " " + message;			
			postStatusUpdate();
			break;
		}
	}//END onCLick method

	

	private void postStatusUpdate()
	{
		Session session = Session.getActiveSession();
		pd = CommonUtils.getProgressDialog(this);
		pd.show();
		Bitmap bm  = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher, null);
		Request photoRequest = Request.newUploadPhotoRequest(Session.getActiveSession(), bm,
				new Request.Callback() {
			
			@Override
			public void onCompleted(Response response)
			{
				showPublishResult();

			}
		});

		Bundle parameters = photoRequest.getParameters();

		parameters.putString("message", message);
		if(session != null)
			photoRequest.executeAsync();

	}//END postStatusUpdate method
	


	private void showPublishResult() 
	{	
		if(pd != null)
			pd.cancel();
		DialogGenerator dg = new DialogGenerator(this);
		dg.generateDateWarningDialog(alertMsg);
	}
	
}
