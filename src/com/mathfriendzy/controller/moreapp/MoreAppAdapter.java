package com.mathfriendzy.controller.moreapp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.readingfriendzypaid.R;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.ICommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MoreAppAdapter extends BaseAdapter{

	private ArrayList<AppDetail> appDetailList = null;
	private AppClickListener lister = null;
	private LayoutInflater mInflator = null;
	private ViewHolder vHolder = null;
	private DisplayImageOptions options;
	private ImageLoader loader = null;
	private ArrayList<String> installedPackageList = null;
	private String btnTitlePlayAgain = null;
	private String btnTitleTryFree = null;
	
	public MoreAppAdapter(Context context , 
			ArrayList<AppDetail> appDetailList , 
			ArrayList<String> installedPackageList,
			AppClickListener lister
			, String playAgainTxt , String freeTxt){
		this.appDetailList = appDetailList;
		this.lister = lister;
		mInflator = LayoutInflater.from(context);
		loader = MathFriendzyHelper.getImageLoaderInstance();
		options = MathFriendzyHelper.getDisplayOptionWithoutLoadingFail();
		this.installedPackageList = installedPackageList;
		this.btnTitlePlayAgain = playAgainTxt;
		this.btnTitleTryFree = freeTxt;
	}

	@Override
	public int getCount() {
		return appDetailList.size();
	}

	@Override
	public Object getItem(int position) {		
		return null;
	}

	@Override
	public long getItemId(int position) {		
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		if(view == null){
			view = mInflator.inflate(R.layout.more_app_view_app_layout, null);
			vHolder = new ViewHolder();
			vHolder.appIcon = (ImageView) view.findViewById(R.id.appIcon);
			vHolder.txtAppName = (TextView) view.findViewById(R.id.txtAppName);
			vHolder.btnType = (Button) view.findViewById(R.id.btnType);
			view.setTag(vHolder);
		}else{
			vHolder = (ViewHolder) view.getTag();
		}
		vHolder.txtAppName.setText(appDetailList.get(position).getAppName());
		this.setButtonText(vHolder.btnType , appDetailList.get(position).getPackageName());
		this.displayImage(ICommonUtils.DOWNLOAD_APP_ICON_URL + this
				.getIconName(appDetailList.get(position).getIconImage()), vHolder.appIcon);
		vHolder.btnType.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lister.OnClickListener(appDetailList.get(position));
			}
		});

		vHolder.appIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lister.OnClickListener(appDetailList.get(position));
			}
		});
		return view;
	}

	private String getIconName(String iconName){
		if(iconName.contains(".png"))
			return iconName;
		return iconName + ".png";
	}

	private void displayImage(String url , ImageView imgView){
		loader.displayImage(url, imgView, options, null);
	}

	private void setButtonText(Button btn , String packageName){
		if(MathFriendzyHelper.isEmpty(packageName)){
			btn.setText(btnTitleTryFree);
		}else{
			if(installedPackageList.contains(packageName)){
				btn.setText(btnTitlePlayAgain);
			}else{
				btn.setText(btnTitleTryFree);
			}
		}
	}

	private class ViewHolder{
		ImageView appIcon;
		TextView txtAppName;
		Button btnType;
	}
}
