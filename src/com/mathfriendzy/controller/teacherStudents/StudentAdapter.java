package com.mathfriendzy.controller.teacherStudents;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.readingfriendzypaid.R;

/**
 * This adapter set the teacher player to the teacher player Activity when user login as a teacher
 * @author Yashwant Singh
 *
 */
public class StudentAdapter extends BaseAdapter
{
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private ArrayList<String> studentNameList  = null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	
	public StudentAdapter(Context context,  int resource , ArrayList<String> studentNameList)
	{
		this.resource 	= resource;
		mInflater 		= LayoutInflater.from(context);
		this.context 	= context;
		this.studentNameList = studentNameList;
	}
	
	@Override
	public int getCount() 
	{
		return studentNameList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup viewGroup) 
	{
		if(view == null)
		{
			vholder 			   = new ViewHolder();
			view 				   = mInflater.inflate(resource, null);
			vholder.txtStudentName = (TextView) view.findViewById(R.id.txtStudentName);
			view.setTag(vholder);			
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
		
		vholder.txtStudentName.setText(studentNameList.get(index));
		
		return view;
	}

	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtStudentName ;
		ImageView imgDetail ;
	}
}
