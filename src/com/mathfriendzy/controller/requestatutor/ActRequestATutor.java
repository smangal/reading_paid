package com.mathfriendzy.controller.requestatutor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.readingfriendzypaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

public class ActRequestATutor extends AdBaseActivity implements OnClickListener{
		
	private final String TAG = this.getClass().getSimpleName();
	
	//For Non Subscribe USer Layout
    private TextView txtNeedHelpWithMath = null;
    private TextView txtTalkToLiveTutor = null;
    private TextView txtProfessionalTutorsForNonSubscribeUser = null;
    private TextView txtDownloadOurSisterApp = null;
    private Button btnTryitFreeNowForNonSubscribeUser = null;
    private TextView txtFreeTutorsForNonSuscribeUser = null;
    private TextView txtStudentInYourSchool = null;
    private Button btnViewDetailForNonSuscribeUser = null;
    private TextView txtTopbar = null;
    	
    private TextView txtRequestATutor = null;
    private TextView txtChooseOptionFrom = null;
    private TextView btnTeacherFriends = null;
    private TextView txtTeacherFriends = null;
    private TextView btnFreeSchoolTutors = null;
    private TextView txtFreeSchoolTutors = null;
    private TextView btnProfessionalTutors = null;
    private TextView txtProfessionalTutors = null;
    private TextView txtAllTheseAreMade = null;
    private Button btnInviteFriends = null;
    
    private final String SCHOOL_APP_LINK = "https://play.google.com/store/apps/details?id=com.homework_friendzy";
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_request_atutor);
		
		CommonUtils.printLog(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.setTranslationTextForWidgets();
		this.setListenerOnWidgets();
		
		CommonUtils.printLog(TAG, "outside onCreate()");
	}
	
	private void setWidgetsReferences(){
		txtTopbar = (TextView) findViewById(R.id.txtTopbar);
		txtNeedHelpWithMath = (TextView) findViewById(R.id.txtNeedHelpWithMath);
        txtTalkToLiveTutor = (TextView) findViewById(R.id.txtTalkToLiveTutor);
        txtProfessionalTutorsForNonSubscribeUser = (TextView) findViewById(R.id.txtProfessionalTutorsForNonSubscribeUser);
        txtDownloadOurSisterApp = (TextView) findViewById(R.id.txtDownloadOurSisterApp);
        btnTryitFreeNowForNonSubscribeUser = (Button) findViewById(R.id.btnTryitFreeNowForNonSubscribeUser);
        txtFreeTutorsForNonSuscribeUser = (TextView) findViewById(R.id.txtFreeTutorsForNonSuscribeUser);
        txtStudentInYourSchool = (TextView) findViewById(R.id.txtStudentInYourSchool);
        btnViewDetailForNonSuscribeUser = (Button) findViewById(R.id.btnViewDetailForNonSuscribeUser);
               
        txtRequestATutor = (TextView) findViewById(R.id.txtRequestATutor);
        txtChooseOptionFrom = (TextView) findViewById(R.id.txtChooseOptionFrom);
        btnTeacherFriends = (TextView) findViewById(R.id.btnTeacherFriends);
        txtTeacherFriends = (TextView) findViewById(R.id.txtTeacherFriends);
        btnFreeSchoolTutors = (TextView) findViewById(R.id.btnFreeSchoolTutors);
        txtFreeSchoolTutors = (TextView) findViewById(R.id.txtFreeSchoolTutors);
        btnProfessionalTutors = (TextView) findViewById(R.id.btnProfessionalTutors);
        txtProfessionalTutors = (TextView) findViewById(R.id.txtProfessionalTutors);
        txtAllTheseAreMade = (TextView) findViewById(R.id.txtAllTheseAreMade);
        btnInviteFriends = (Button) findViewById(R.id.btnInviteFriends);
    } 

	private void setTranslationTextForWidgets(){
		Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        txtNeedHelpWithMath.setText(transeletion.getTranselationTextByTextIdentifier("lblNeedHelpWithMath"));
        txtTalkToLiveTutor.setText(transeletion.getTranselationTextByTextIdentifier("lblTalkToLiveTutor"));
        txtProfessionalTutorsForNonSubscribeUser.setText
                (transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtDownloadOurSisterApp.setText(transeletion.getTranselationTextByTextIdentifier("lblDownloadSisterApp"));
        btnTryitFreeNowForNonSubscribeUser
                .setText(transeletion.getTranselationTextByTextIdentifier("titleTryItFree"));
        txtFreeTutorsForNonSuscribeUser.setText(transeletion
                .getTranselationTextByTextIdentifier("lblFreeTutors"));
        txtStudentInYourSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblStudentsInYourSchool"));
        btnViewDetailForNonSuscribeUser.setText(transeletion
                .getTranselationTextByTextIdentifier("titleViewDetails"));
        txtRequestATutor.setText(transeletion.getTranselationTextByTextIdentifier("lblRequestATutor"));
        txtChooseOptionFrom.setText(transeletion.getTranselationTextByTextIdentifier("lbl3Options"));
        btnTeacherFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher")
                + "/" + transeletion.getTranselationTextByTextIdentifier("lblFriends"));
        txtTeacherFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblBeTutoredFree"));
        btnFreeSchoolTutors.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool") + " " 
        		+ transeletion.getTranselationTextByTextIdentifier("lblTutor"));
        txtFreeSchoolTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblBeTutoredByTutor"));
        btnProfessionalTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblProfessionalTutors"));
        txtProfessionalTutors.setText(transeletion.getTranselationTextByTextIdentifier("lblBeTutorProf"));
        txtAllTheseAreMade.setText(transeletion.getTranselationTextByTextIdentifier("lblAllThesePossible"));
        btnInviteFriends.setText(transeletion.getTranselationTextByTextIdentifier("lblTryNowItFree"));
        transeletion.closeConnection();
    }
	
    protected void setListenerOnWidgets() {
    	CommonUtils.printLog(TAG , "inside setListenerOnWidgets()"); 
        btnTryitFreeNowForNonSubscribeUser.setOnClickListener(this);
        btnViewDetailForNonSuscribeUser.setOnClickListener(this);
        btnInviteFriends.setOnClickListener(this);
        CommonUtils.printLog(TAG , "outside setListenerOnWidgets()");
    }
    
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnTryitFreeNowForNonSubscribeUser:			
			MathFriendzyHelper.openUrl(this, MathFriendzyHelper.MATH_TUTOR_PLUS_LINK);
			break;
		case R.id.btnViewDetailForNonSuscribeUser:
			startActivity(new Intent(this , ActViewDetailForProfessionalTutor.class));
			break;
		case R.id.btnInviteFriends:
			MathFriendzyHelper.openUrl(this, SCHOOL_APP_LINK);
			break;
		}
	}   
}
