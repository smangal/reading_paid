package com.mathfriendzy.controller.learningcenter.equationsolve;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.learningcenter.CongratulationActivity;
import com.mathfriendzy.controller.learningcenter.SeeAnswerActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.database.LocalPlayerDatabase;
import com.mathfriendzy.model.friendzy.SaveTimePointsForFriendzyChallenge;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.learningcenter.triviaEquation.TriviaQuestion;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.PlaySound;
import com.readingfriendzypaid.R;


public class LearningCentreEquationForGrade extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen						= null;
	private TextView txtTime								= null;
	private TextView txtPlayername							= null;
	private TextView txtPoints								= null;
	private TextView txtQueNo								= null;
	private TextView txtQue									= null;
	private TextView txtOption1								= null;
	private TextView txtOption2								= null;
	private TextView txtOption3								= null;
	private TextView txtOption4								= null;

	private LinearLayout progressBarLayout					= null;
	private RelativeLayout ansLayout1						= null;
	private RelativeLayout ansLayout2						= null;
	private RelativeLayout ansLayout3						= null;
	private RelativeLayout ansLayout4						= null;
	private ImageView imgFlag								= null;
	private ImageView imgProgressBar						= null;

	private ArrayList<LearningCenterTransferObj> categories	= null;
	private ArrayList<String> optionArray					= null;
	private LearningCenterTransferObj questionObj 			= null;

	private int questionNumber								= 0;
	private int id											= -1;
	private final int MAX_QUESTION							= 10;
	private CountDownTimer timer							= null;
	private long START_TIME									= 2*60*1000;
	private final long INTERVAL								= 1*1000;
	private static long playingTime							= 0;
	private int operation_id								= 0;
	private int levels_id									= 0;
	private String strPoint									= null;
	private String title									= null;
	private int score										= 0;

	//Defining two integer variable for storing play start_time and end_time
	private int start_time									= 0;
	private int end_time									= 0;
	private static boolean timeFlag							= false;
	private long DISPLAY_TIME 								= 0;
	//Max and mininmum points for a correct answer
	private final int MIN_SCORE								= 225;
	private final int MAX_SCORE								= 250;
	private boolean flag_right								= false;

	private int points 										= 0;
	private int pointsForEachQuestion 						= 0;

	private int rightAnsCounter  							= 0;
	private int numberOfStars     							= 0;
	private int numberOfCoins     							= 0;


	private float coinsPerPoint   							= 0.05f;
	private int maxCoinsAfterPlayTenLevel 					= 200;//User can get 200 bonus coins after play all 10 level 

	private boolean isClickOnBackPressed 					= false;
	public static SeeAnswerTransferObj seeAnswerDataObj 	= null;
	private ArrayList<String> userAnswerList				= null;
	private String userAnswer 								= "";
	private int isCorrectAnsByUser 							= 0;
	private int totalTimeTaken								= 0;
	private LearningCenterTransferObj learnignCenterobj		= null;
	private ArrayList<MathScoreTransferObj> playDatalist 	= null;

	private Date startTime 									= null;
	private Date endTime   									= null;
	private PlaySound	playSound							= null;
	private boolean isTabSmall								= false;
	//private boolean isTabLarge								= false;

	//Change for time issue
	private final int EXTRA_PLAY_TIME						= 30;
	private final int MIN_PLAY_TIME							= 90;
	private boolean isClick									= false;
	private Date holdingTime								= null;
	//ENd Change


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning_centre_equation_solve);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{

			setContentView(R.layout.activity_learning_center_equation_low_tab);
		}

		isTabSmall = getResources().getBoolean(R.bool.isTabSmall);
		//isTabLarge = getResources().getBoolean(R.bool.isTabLarge);

		operation_id = getIntent().getIntExtra("operationId", 0);
		levels_id	 = getIntent().getIntExtra("level", 0);

		playSound	 = new PlaySound(); 
		playSound.playSoundForBackground(this);

		timeFlag			= false;
		TriviaQuestion db 	= new TriviaQuestion(this);
		seeAnswerDataObj 	= new SeeAnswerTransferObj();
		userAnswerList		= new ArrayList<String>();
		learnignCenterobj	= new LearningCenterTransferObj();
		playDatalist        = new ArrayList<MathScoreTransferObj>();

		categories			= new ArrayList<LearningCenterTransferObj>();
		categories			= db.getQuestionList(levels_id, operation_id);

		getWidgetId();	
		title = CommonUtils.setLearningTitle(operation_id);
		setWidgetListener();
		setTimer();		
		setPlayerDetails();
		getNewQuestion();		
		setPoints();

		this.setTimerVisibility();
	}//END onCreate method


	private void setTimerVisibility(){
		txtTime.setVisibility(TextView.GONE);
	}
	
	private void setTimer()
	{
		timer = new MyCountDownTimer(START_TIME, INTERVAL);
		timer.start();		
	}



	/**
	 * sets point on UI
	 */
	private void setPoints()
	{			
		points = getScore();
		txtPoints.setText(strPoint+" : "+CommonUtils.setNumberString(points+""));
		start_time = (int) (new Date().getTime()/1000);

	}//END setPoints method



	private int getScore() 
	{
		end_time = (int) (new Date().getTime()/1000);

		if(start_time == 0)
		{
			return 0;
		}
		else
		{
			score = points + (MAX_SCORE - (5 * (end_time - start_time)));
			pointsForEachQuestion = MAX_SCORE - (5 * (end_time - start_time));

			if(pointsForEachQuestion < MIN_SCORE)
				return points + MIN_SCORE;

			return score;
		}

	}



	/**
	 * To reuse this method on selection of answer
	 */
	private void getNewQuestion() 
	{		
		isClick = false;

		setClickckableTrue();
		startTime  = new Date();
		questionObj = new LearningCenterTransferObj();
		questionObj = categories.get(questionNumber);
		setOptionArray(questionObj);
		questionNumber++;
		visibleOptions();
		setWidgetText();	
		setOptionBackground();
		learnignCenterobj = questionObj;
	}//END getNewQuestion method



	@SuppressWarnings("deprecation")
	private void setPlayerDetails() 
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));

		txtPlayername.setText(playerName+".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(
				sharedPreffPlayerInfo.getString("countryName", ""), this);
		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/"	+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

	}//END setPlayerDetail Method



	private void setOptionBackground()
	{
		if(isTabSmall)
		{
			ansLayout1.setBackgroundResource(R.drawable.option1_ipad);
			ansLayout2.setBackgroundResource(R.drawable.option2_ipad);
			ansLayout3.setBackgroundResource(R.drawable.option3_ipad);
			ansLayout4.setBackgroundResource(R.drawable.option4_ipad);
		}
		/*else if(isTabLarge)
		{
			ansLayout1.setBackgroundResource(R.drawable.option1_ipad2);
			ansLayout2.setBackgroundResource(R.drawable.option2_ipad2);
			ansLayout3.setBackgroundResource(R.drawable.option3_ipad2);
			ansLayout4.setBackgroundResource(R.drawable.option4_ipad2);
		}*/
		else
		{
			ansLayout1.setBackgroundResource(R.drawable.option1);
			ansLayout2.setBackgroundResource(R.drawable.option2);
			ansLayout3.setBackgroundResource(R.drawable.option3);
			ansLayout4.setBackgroundResource(R.drawable.option4);
		}		

	}//END setOptionsBackground method



	/**
	 * Use to set options in a string array
	 * @param categories 
	 */
	private void setOptionArray(LearningCenterTransferObj categories) 
	{
		optionArray	= new ArrayList<String>();
		optionArray.add(categories.getAnswer());
		optionArray.add(categories.getOption1());
		optionArray.add(categories.getOption2());
		optionArray.add(categories.getOption3());		
	}//END setOptionArray method


	/**
	 * use to set Text on UI
	 * @param questionObj 
	 */
	private void setWidgetText() 
	{
		Translation translate = new Translation(this);
		translate.openConnection();		
		mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier(title));		
		strPoint = (translate.getTranselationTextByTextIdentifier("lblPts"));
		translate.closeConnection();

		txtQueNo.setText(""+questionNumber);
		txtQue.setText(questionObj.getQuestion());
		id = new Random().nextInt(optionArray.size());
		txtOption1.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption2.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption3.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption4.setText(optionArray.get(id));
		optionArray.remove(id);

	}//END setWidgetId method


	/**
	 * use to get id of the widgets
	 */
	private void getWidgetId()
	{
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtOption1			= (TextView) findViewById(R.id.txtOption1);
		txtOption2			= (TextView) findViewById(R.id.txtOption2);
		txtOption3			= (TextView) findViewById(R.id.txtOption3);
		txtOption4			= (TextView) findViewById(R.id.txtOption4);
		txtPlayername		= (TextView) findViewById(R.id.txtPlayerName);
		txtPoints			= (TextView) findViewById(R.id.txtPoints);
		txtQue				= (TextView) findViewById(R.id.txtQue);
		txtQueNo			= (TextView) findViewById(R.id.txtQueNo);
		txtTime				= (TextView) findViewById(R.id.txtTime);
		imgFlag				= (ImageView) findViewById(R.id.imgFlag);

		progressBarLayout	= (LinearLayout) findViewById(R.id.progressBarLayout);
		ansLayout1			= (RelativeLayout) findViewById(R.id.ansLayout1);
		ansLayout2			= (RelativeLayout) findViewById(R.id.ansLayout2);
		ansLayout3			= (RelativeLayout) findViewById(R.id.ansLayout3);
		ansLayout4			= (RelativeLayout) findViewById(R.id.ansLayout4);

	}



	/**
	 * use to setListener for widget
	 */
	private void setWidgetListener()
	{
		ansLayout1.setOnClickListener(this);
		ansLayout2.setOnClickListener(this);
		ansLayout3.setOnClickListener(this);
		ansLayout4.setOnClickListener(this);

	}//END setWidgetListner method


	/**
	 * disable listener when all questions completed.
	 */
	private void visibleOptions() 
	{
		ansLayout1.setVisibility(View.VISIBLE);
		ansLayout2.setVisibility(View.VISIBLE);
		ansLayout3.setVisibility(View.VISIBLE);
		ansLayout4.setVisibility(View.VISIBLE);
	}//END disableListner method


	private void setClickckableTrue()
	{
		ansLayout1.setEnabled(true);
		ansLayout2.setEnabled(true);
		ansLayout3.setEnabled(true);
		ansLayout4.setEnabled(true);
	}

	private void setClickckableFalse()
	{
		ansLayout1.setEnabled(false);
		ansLayout2.setEnabled(false);
		ansLayout3.setEnabled(false);
		ansLayout4.setEnabled(false);
	}


	@Override
	public void onClick(View v)
	{		
		playSound.playSoundForSelectingAnswer(this);
		isClick = true;

		setClickckableFalse();
		String layoutText	= null;
		imgProgressBar = new ImageView(this);
		ansLayout1.setVisibility(View.INVISIBLE);
		ansLayout2.setVisibility(View.INVISIBLE);
		ansLayout3.setVisibility(View.INVISIBLE);
		ansLayout4.setVisibility(View.INVISIBLE);
		v.setVisibility(View.VISIBLE);
		switch (v.getId()) 
		{
		case R.id.ansLayout1:
			if(isTabSmall)
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right_ipad);		
			}
			/*else if(isTabLarge)
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right_ipad2);		
			}*/
			else
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right);		
			}				
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption1);
			break;

		case R.id.ansLayout2:
			if(isTabSmall)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad2);
			}*/
			else
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right);
			}			
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption2);
			break;

		case R.id.ansLayout3:
			if(isTabSmall)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad2);
			}*/
			else
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right);
			}			
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption3);
			break;

		case R.id.ansLayout4:
			if(isTabSmall)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad2);
			}*/
			else
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right);
			}			
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption4);
			break;		
		}

		checkAnswer(layoutText, v);		

	}//END onClick Method



	/**
	 * use to check answer and update UI with new Question
	 * @param layoutText refer to selected text for answer
	 */
	private void checkAnswer(String layoutText, final View v) 
	{			
		userAnswer = layoutText;

		if(layoutText.equals(questionObj.getAnswer()))
		{	
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
			}
			/*else if(isTabLarge)
			{
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad2);
			}*/
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.green);
			}	
			flag_right = true;			
			setPoints();
			DISPLAY_TIME = 500;
		}
		else 
		{
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
			}
			/*else if(isTabLarge)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
			}*/
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.red);
			}	
			flag_right = false;
			DISPLAY_TIME = 1200;				
		}

		progressBarLayout.addView(imgProgressBar);

		//playSound.playSoundForAnimation(getApplicationContext());

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{
				if(!flag_right)
				{
					playSound.playSoundForWrong(getApplicationContext());
					if(isTabSmall)
					{
						v.setBackgroundResource(R.drawable.wrong_ipad);
					}
					/*else if(isTabLarge)
					{
						v.setBackgroundResource(R.drawable.wrong_ipad2);
					}*/
					else
					{
						v.setBackgroundResource(R.drawable.wrong);
					}	

					isCorrectAnsByUser = 0;					
				}
				else
				{
					playSound.playSoundForRight(getApplicationContext());
					rightAnsCounter ++ ;
					isCorrectAnsByUser = 1;	
				}			
				//savePlayData();
				int right_bg_id;
				if(isTabSmall)
				{
					right_bg_id = R.drawable.right_ipad;
				}
				/*else if(isTabLarge)
				{
					right_bg_id = R.drawable.right_ipad2;
				}*/
				else
				{
					right_bg_id = R.drawable.right;
				}
				checkRightOption(right_bg_id);

			}

		}, DISPLAY_TIME);


		if(questionNumber == MAX_QUESTION)
		{
			playSound.playSoundForResultScreen(getApplicationContext());				
		}

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{

				if(questionNumber < MAX_QUESTION)
				{	
					savePlayData();
					getNewQuestion();
				}
				else
				{
					setCoinsAndPointsAfterSolveAllEquation();
					Intent intent = new Intent(LearningCentreEquationForGrade.this, CongratulationActivity.class);
					intent.putExtra("points", score);
					startActivity(intent);
				}

			}

		}, 2000);

	}//END checkAnswer method


	/**
	 * use to check which layout contains the right answer
	 * @param backgroungId
	 */
	private void checkRightOption(int backgroungId)
	{
		if(MathFriendzyHelper.returnTextFromTextview(txtOption1).equals(questionObj.getAnswer()))
		{
			ansLayout1.setVisibility(View.VISIBLE);
			ansLayout1.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption2).equals(questionObj.getAnswer()))
		{
			ansLayout2.setVisibility(View.VISIBLE);
			ansLayout2.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption3).equals(questionObj.getAnswer()))
		{
			ansLayout3.setVisibility(View.VISIBLE);
			ansLayout3.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption4).equals(questionObj.getAnswer()))
		{
			ansLayout4.setVisibility(View.VISIBLE);
			ansLayout4.setBackgroundResource(backgroungId);
		}
	}//END checkRightOption method



	/**
	 * timer class for playing game
	 * @author Shilpi Mangal
	 *
	 */
	private class MyCountDownTimer extends CountDownTimer
	{
		public MyCountDownTimer(long startTime, long interval)
		{
			super(startTime, interval);
		}

		@Override
		public void onFinish() 
		{
			isClickOnBackPressed = false;
			//callOnBackPressed();
			setCoinsAndPointsAfterSolveAllEquation();
			Intent intent = new Intent(LearningCentreEquationForGrade.this, CongratulationActivity.class);
			intent.putExtra("points", score);
			startActivity(intent);
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{
			playingTime	= millisUntilFinished/1000;
			txtTime.setText(DateTimeOperation.setTimeInMinAndSec(playingTime));	
		}
	}//END MyCountDownTimer class

	@Override
	protected void onStop() 
	{
		playSound.stopPlayer();
		timer.cancel();
		super.onStop();
	}

	@Override
	protected void onPause() 
	{		
		playSound.stopPlayer();
		timeFlag = true;
		timer.cancel();
		super.onPause();
	}

	@Override
	protected void onRestart() 
	{		
		playSound = new PlaySound();
		playSound.playSoundForBackground(this);

		if(timeFlag)
		{
			START_TIME = playingTime * 1000;
			setTimer();
		}
		super.onRestart();
	}

	@Override
	public void onBackPressed() 
	{
		timer.cancel();
		isClickOnBackPressed = false;
		numberOfCoins = (int) (points * coinsPerPoint);
		this.callOnBackPressed();
		super.onBackPressed();
	}



	/**
	 * This method call when user click on back pressed and also after the completion of time counter
	 */
	private void callOnBackPressed()
	{	
		SeeAnswerActivity.isCallFromActivity	= 1;

		if(!isClickOnBackPressed)
			savePlayData();


		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{	
			//For Temp Player...
			this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			Intent intent = new Intent();
			setResult(RESULT_OK,intent);

		}
		else
		{
			//For login user..
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				if(!isClickOnBackPressed)
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTempPlayerScoreOnServer(savePlayDataToDataBase()).execute(null,null,null);
				}
				else
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, LearningCentreEquationForGrade.this)
					.execute(null, null, null);
				}
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			}
		}
	}


	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setGameType("Play");
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setIsWin(0);	

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playDatalist));

		mathResultObj.setTotalScore(points);		
		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}


	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}


	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			//Log.e("LearningCenter", "Player Points Exist");
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
					+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}
		else
		{
			Log.e("LearningCenter", "Player Points Not Exist");
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}


	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(points);
		playerPoints.setCoins(numberOfCoins);
		playerPoints.setCompleteLevel(1);
		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}

	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{

		private MathResultTransferObj mathobj = null;

		SaveTempPlayerScoreOnServer(MathResultTransferObj mathObj)
		{			
			this.mathobj = mathObj;
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCentreEquationForGrade.this);
			register.savePlayerScoreOnServerForloginuser(mathobj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
			new AddCoinAndPointsForLoginUser(getPlayerPoints(mathobj)).execute(null,null,null);
		}
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void> 
	{		
		private PlayerTotalPointsObj playerObj = null;
		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCentreEquationForGrade.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCentreEquationForGrade.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			new SaveTimePointsForFriendzyChallenge(totalTimeTaken, points, LearningCentreEquationForGrade.this)
			.execute(null, null, null);
			super.onPostExecute(result);

		}
	}

	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class AddMathPlayerLavelScoreOnServer extends AsyncTask<Void, Void, Void>
	{
		private MathResultTransferObj mathobj = null;

		AddMathPlayerLavelScoreOnServer(MathResultTransferObj mathObj)
		{			
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(LearningCentreEquationForGrade.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(mathObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.mathobj = mathObj;
			//Log.e("Coins", ""+mathObj.getCoins());
			this.mathobj.setCoins(playerObj.getCoins());
			this.mathobj.setTotalPoints(playerObj.getTotalPoints());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(LearningCentreEquationForGrade.this);
			register.addMathPlayLevelScore(mathobj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
		}
	}


	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playDatalist)
	{

		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playDatalist.size() ; i++)
		{
			xml.append("<equation>" +
					"<equationId>"+playDatalist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playDatalist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playDatalist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					/*"<lap>"+playDatalist.get(i).getLap()+"</lap>"+
					"<time_taken_to_answer>"+playDatalist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+*/
					"<math_operation_id>"+playDatalist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playDatalist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playDatalist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playDatalist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}



	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */		
	@SuppressWarnings("deprecation")
	private void savePlayData()
	{		
		int extraSec = 0;
		if(endTime == null){
			holdingTime = startTime;			
		}else{
			holdingTime = endTime;
		}

		endTime	 = new Date();		

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operation_id);
		mathObj.setEquationId(questionObj.getEquationId());

		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);


		/*Change for save time extra after stop clicking on answer*/
		
		//Log.e("", "holdingTime"+holdingTime);
		//Log.e("", "holdingTime"+endTime);
		
		if(totalTimeTaken < MIN_PLAY_TIME && !isClick 
				&& DateTimeOperation.isValidForChange(holdingTime, endTime)){

			endTime = startTime;
			String time = DateTimeOperation.getEndTimeAfterAddedExtraTime(CommonUtils.formateDate(endTime),
					EXTRA_PLAY_TIME);
			mathObj.setEndDateTimeStr(time);

			//Log.e("time", ""+time);

			extraSec = EXTRA_PLAY_TIME;

		}//End change



		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if( endTime.getSeconds() < startTime.getSeconds())
			endTimeVlaue = endTimeVlaue + 60;		

		totalTimeTaken = totalTimeTaken + (endTimeVlaue - startTimeValue) +extraSec;		

		//Log.e("", "totalTimeTaken"+totalTimeTaken);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer(" ");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);


		mathObj.setTimeTakenToAnswer(totalTimeTaken);
		mathObj.setEquation(learnignCenterobj);

		pointsForEachQuestion = 0;
		playDatalist.add(mathObj);

		userAnswerList.add(userAnswer);
	}

	


	/**
	 * set coins and points with star on level
	 */
	private void setCoinsAndPointsAfterSolveAllEquation()
	{
		/*if(questionNumber == 10) // if user play minimum 10 equation the star will count otherwise not
		{*/

		savePlayData();
		int percentage = (rightAnsCounter * 100 )/questionNumber; // calculate the percentage

		//calculate number of star
		if(percentage >= 50 && percentage <=75)
			numberOfStars = 1;
		else if(percentage >= 76 && percentage <=89)
			numberOfStars = 2;
		else if(percentage >= 90 && percentage <=100)
			numberOfStars = 3;
		//}

		MathResultTransferObj mathObj = savePlayDataToDataBase();

		PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();
		playerEquationObj.setUserId(mathObj.getUserId());
		playerEquationObj.setPlayerId(mathObj.getPlayerId());

		playerEquationObj.setLevel(levels_id);
		playerEquationObj.setStars(numberOfStars);
		playerEquationObj.setEquationType(operation_id);
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(LearningCentreEquationForGrade.this);
		learningCenterObj.openConn();

		numberOfCoins = (int) (points * coinsPerPoint);//calculate number of coins

		int highestLevel =  learningCenterObj.getHighetsLevel(playerEquationObj);

		//9 is the highest level , and 10 is the level playing at current
		if(highestLevel == 9 && levels_id == 10 && numberOfStars > 0)
			numberOfCoins = numberOfCoins + maxCoinsAfterPlayTenLevel;//add 2000 coins when user play all 10 levels
		//Log.e("numberOfCoins", ""+numberOfCoins);
		isClickOnBackPressed = true;
		callOnBackPressed();		

		if(numberOfStars > 0)
		{			
			if(learningCenterObj.isEquationPlayerExist(mathObj.getUserId(), 
					mathObj.getPlayerId(), levels_id, operation_id)){

				learningCenterObj.updateEquationPlayer(mathObj.getUserId(),
						mathObj.getPlayerId(), levels_id, operation_id,numberOfStars);
			}
			else
			{				
				learningCenterObj.insertIntoPlayerEquationLevel(playerEquationObj);
			}
		}

		int earnPoints = points;

		mathObj.setTotalPoints(points);//this point will update at the time of update on server

		learningCenterObj.closeConn();		

		mathObj.setTotalScore(points);
		mathObj.setEquationTypeId(operation_id);
		mathObj.setLevel(levels_id);
		mathObj.setStars(numberOfStars);

		//mathObj.setTotalPoints(points);//this will be changes when the player information is loaded from server

		mathObj.setCoins(numberOfCoins);


		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{			
			if(CommonUtils.isInternetConnectionAvailable(LearningCentreEquationForGrade.this))
			{
				new AddMathPlayerLavelScoreOnServer(mathObj).execute(null,null,null);
			}
			else
			{
				if(numberOfStars > 0)
				{
					PlayerEquationLevelObj localObj = new PlayerEquationLevelObj();
					localObj.setEquationType(operation_id);
					localObj.setLevel(levels_id);
					localObj.setStars(numberOfStars);
					localObj.setUserId(mathObj.getUserId());
					localObj.setPlayerId(mathObj.getPlayerId());
					LocalPlayerDatabase db = new LocalPlayerDatabase();
					db.openConn(this);				
					db.insertIntoPlayerEquationLevel(localObj);
					db.closeConn();
				}
			}

		}


		//set data for transferring to the answer list
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		seeAnswerDataObj.setPlayerName(sharedPreffPlayerInfo.getString("playerName", ""));
		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo
				.getString("countryName", ""), LearningCentreEquationForGrade.this);

		seeAnswerDataObj.setNoOfCorrectAnswer(rightAnsCounter);
		seeAnswerDataObj.setLevel(levels_id);
		seeAnswerDataObj.setPoints(earnPoints);
		seeAnswerDataObj.setCountryISO(coutryIso);
		seeAnswerDataObj.setEquationList(categories);

		seeAnswerDataObj.setUserAnswerList(userAnswerList);

		//end
	}

}
