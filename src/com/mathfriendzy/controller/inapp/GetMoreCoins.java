package com.mathfriendzy.controller.inapp;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.readingfriendzypaid.R;
import com.mathfriendzy.controller.ads.AdsServerOperation;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.coinsdistribution.CoinsDistributionActivity;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.inapp.IabHelper;
import com.mathfriendzy.inapp.IabHelper.IabAsyncInProgressException;
import com.mathfriendzy.inapp.IabResult;
import com.mathfriendzy.inapp.Inventory;
import com.mathfriendzy.inapp.Purchase;
import com.mathfriendzy.model.inapp.CoinsPacktransferObj;
import com.mathfriendzy.model.inapp.InAppServerOperation;
import com.mathfriendzy.model.inapp.InnAppImpl;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.HttpResponseInterface;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;

import java.io.IOException;
import java.util.ArrayList;

import static com.mathfriendzy.utils.ICommonUtils.GET_MORE_COINS_FLAG;

/**
 * This activity is open when user want to purchse coins
 * @author Yashwant Singh
 *
 */
public class GetMoreCoins extends AdBaseActivity
{
	private TextView labelTop 		 = null;
	private TextView lblGetMoreCoins = null;
	private TextView txtYourCoins    = null;
	private Button   btnCoinsPack    = null;
	private Button   btnFreeCoins    = null;
	private RelativeLayout coinsPackLayout = null;
	private RelativeLayout freeCoinsLayout = null;

	//for ads
	private TextView txtAdsText      = null;

	private ArrayList<Button> btnPriceList = null;
	private ArrayList<Button> btnPriceFreeList = null;

	private ImageView imgCoins 	= null;
	private TextView txtPackName= null;
	private TextView txtCoins   = null;
	private Button    btnPrice  = null;
	private int userCoins = 0;
	private RegistereUserDto regUserObj = null;
	private ArrayList<CoinsPacktransferObj> coinsPackLPriceList = null;
	private ArrayList<CoinsPacktransferObj> coinsPackLFreeList  = null;

	public static boolean isFromEditRegisteredUserPlayer = false;

	private String selectedPlayerId = null;

	//product IDs
	static final String SKU_AMATEUR_CHALLENGER     	= "amateur_challeng";
	//static final String SKU_AMATEUR_CHALLENGER     	= "android.test.purchased";
	//added
	static final String SKU_CASUAL_CHALLENGER    		= "casual_challeng";
	static final String SKU_SIROUS_CHALLENGER    		= "sirous_challeng";
	static final String SKU_EXTREME_CHALLENGER   		= "extreme_challeng";
	static final String SKU_PROFESSIONAL_CHALLENGER   	= "professional_challeng";
	static final String SKU_COLOSSAL_CHALLENGER   		= "colossal_challeng";

	// The helper object
	IabHelper mHelper;
	//payload
	private String iabPayloadSend;

	private final String TAG = this.getClass().getSimpleName();
	//for ads
	private boolean isShowScrennAfterAds = false;


	//new In-App Changes
	//testing request
	//android.test.purchased
	//android.test.canceled
	public static int VIDEO_PURCHASED = 1;
	public static int UNLOCK_MATH_APP_CATEGORY_PURCHASED = 2;
	public static int DEFAULT_PURCHASED_REQUEST = 0;
	public static String IN_APP_REQUEST = "in_app_request";
	private int appRequest = 0;
	public static final String SKU_VIDEO_PURCHASED = "com.math.video.purchased";
	//public static final String SKU_VIDEO_PURCHASED = "android.test.purchased";
	public static final String SKU_MATH_APP_CATEGORY_PURCHASED = "com.math.math_category.purchased";
	//public static final String SKU_MATH_APP_CATEGORY_PURCHASED = "android.test.purchased";
	private final int PURCHASE_REQUEST_CODE = 1001;
	public static final int START_GET_MORE_COIN_ACTIVITY_REQUEST = 100001;
	private RelativeLayout mainLayout = null;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_more_coins);

		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "inside onClick()");

		this.initializePlayerId();
		this.getIntentValues();
		this.setWidgetsReferences();
		//this.getUserData();
		this.setTextFromTranslation();
		if(appRequest == DEFAULT_PURCHASED_REQUEST) {
			this.getCoinPack();
			this.getUserData();
		}
		this.initializeInAppBuilder();
		this.setVisibilityOfMainLayout();

		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "outside onClick()");
	}

	private void setVisibilityOfMainLayout(){
		if(appRequest != DEFAULT_PURCHASED_REQUEST) {
			mainLayout.setVisibility(RelativeLayout.GONE);
		}
	}

	private void initializePlayerId(){
		if(isFromEditRegisteredUserPlayer){
			isFromEditRegisteredUserPlayer = false;//set in choose avtar click on getMore coins button
			selectedPlayerId = EditRegisteredUserPlayer.playerId;
		}else{
			//changes for friendzy challenge
			SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(ICommonUtils.IS_CHECKED_PREFF, 0);
			selectedPlayerId = sharedPreffPlayerInfo.getString(ICommonUtils.PLAYER_ID, "0");
		}
	}

	private void getCoinPack(){
		if(CommonUtils.isInternetConnectionAvailable(this)){
			new GetCoinsPack().execute(null,null,null);
		}else{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion
					.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}

	private void initializeInAppBuilder(){
		//in app work
		mHelper = new IabHelper(this, ICommonUtils.base64EncodedPublicKey);
		// enable debug logging (for a production application, you should set this to false).
		mHelper.enableDebugLogging(true);
		// Start setup. This is asynchronous and the specified listener
		// will be called once setup completes.
		CommonUtils.printLog(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				CommonUtils.printLog(TAG, "Setup finished.");
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					CommonUtils.printLog(TAG, "Problem setting up in-app billing: " + result);
					return;
				}
				// Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
				CommonUtils.printLog(TAG, "Setup successful. Querying inventory.");
				try {
					mHelper.queryInventoryAsync(mGotInventoryListener);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				startPurchased();
			}
		});
	}


	/**
	 * Get the values which are set in intent at previous screen.
	 */
	private void getIntentValues(){
		isShowScrennAfterAds = this.getIntent().getBooleanExtra("isShowScrennAfterAds", false);
		appRequest = this.getIntent().getIntExtra(IN_APP_REQUEST , 0);
	}

	// Listener that's called when we finish querying the items and subscriptions we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener =
			new IabHelper.QueryInventoryFinishedListener() {

		@Override
		public void onQueryInventoryFinished(IabResult result,
				Inventory inv) {
			if (result.isFailure()) {
				CommonUtils.printLog("TAG", "Failed to query inventory: " + result);
				return;
			}

			try {
				CommonUtils.printLog(TAG, "Query inventory was successful.");

				Purchase purAmateurChallenger = inv.getPurchase(SKU_AMATEUR_CHALLENGER);
				if (purAmateurChallenger != null && verifyDeveloperPayload(purAmateurChallenger)) {
					CommonUtils.printLog(TAG, "We have amatuer challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_AMATEUR_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purCasualChallenger = inv.getPurchase(SKU_CASUAL_CHALLENGER);
				if (purCasualChallenger != null && verifyDeveloperPayload(purCasualChallenger)) {
					CommonUtils.printLog(TAG, "We have casual challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_CASUAL_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purSeriousChallenger = inv.getPurchase(SKU_SIROUS_CHALLENGER);
				if (purSeriousChallenger != null && verifyDeveloperPayload(purSeriousChallenger)) {
					CommonUtils.printLog(TAG, "We have serious challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_SIROUS_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purExtremeChallenger = inv.getPurchase(SKU_EXTREME_CHALLENGER);
				if (purExtremeChallenger != null && verifyDeveloperPayload(purExtremeChallenger)) {
					CommonUtils.printLog(TAG, "We have extreme challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_EXTREME_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purProfessionalChallenger = inv.getPurchase(SKU_PROFESSIONAL_CHALLENGER);
				if (purProfessionalChallenger != null && verifyDeveloperPayload(purProfessionalChallenger)) {
					CommonUtils.printLog(TAG, "We have professional challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_PROFESSIONAL_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purCollosalChallenger = inv.getPurchase(SKU_COLOSSAL_CHALLENGER);
				if (purCollosalChallenger != null && verifyDeveloperPayload(purCollosalChallenger)) {
					CommonUtils.printLog(TAG, "We have Collosal challenger. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_COLOSSAL_CHALLENGER), mConsumeFinishedListener);
					return;
				}

				Purchase purVedioChallenger = inv.getPurchase(SKU_VIDEO_PURCHASED);
				if (purVedioChallenger != null && verifyDeveloperPayload(purVedioChallenger)) {
					CommonUtils.printLog(TAG, "We have Video purchased. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_VIDEO_PURCHASED), mConsumeFinishedListener);
					return;
				}

				Purchase purUnlockMathCategoryChallenger = inv.getPurchase(SKU_MATH_APP_CATEGORY_PURCHASED);
				if (purUnlockMathCategoryChallenger != null && verifyDeveloperPayload(purUnlockMathCategoryChallenger)) {
					CommonUtils.printLog(TAG, "We have Category purchased. Consuming it.");
					mHelper.consumeAsync(inv.getPurchase(SKU_MATH_APP_CATEGORY_PURCHASED), mConsumeFinishedListener);
					return;
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
	};


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		CommonUtils.printLog(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

		// Pass on the activity result to the helper for handling
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		}
		else {
			CommonUtils.printLog(TAG, "onActivityResult handled by IABUtil.");
		}

		if(appRequest == VIDEO_PURCHASED ||
				appRequest == UNLOCK_MATH_APP_CATEGORY_PURCHASED) {
			if (resultCode != RESULT_OK) {
				CommonUtils.printLog(TAG, "onActivityResult Result Fail...");
				finish();
			}
		}
	}

	// We're being destroyed. It's important to dispose of the helper here!
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			// very important:
			CommonUtils.printLog(TAG, "Destroying helper.");
			if (mHelper != null) mHelper.dispose();
			mHelper = null;
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * This method get userDataFrom the database
	 */
	private void getUserData()
	{
		if(!MathFriendzyHelper.isTempraroryPlayerCreated(this)){
			MathFriendzyHelper.createBlankTempTable(this);
		}

		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(!tempPlayer.isTempPlayerDeleted())
		{
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			tempPlayer = new TempPlayerOperation(this);
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(tempPlayer.getTempPlayerData().get(0).getPlayerId() +"");
			tempPlayer.closeConn();
			learningCenterimpl.closeConn();

			userCoins = playerObj.getCoins();
		}
		else
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(this);
			regUserObj = userObj.getUserData();

			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(selectedPlayerId);
			learningCenterimpl.closeConn();

			userCoins = playerObj.getCoins();
		}
	}

	/**
	 * This method set widgets references from layout
	 */
	private void setWidgetsReferences()
	{
		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 		= (TextView) findViewById(R.id.labelTop);
		lblGetMoreCoins = (TextView) findViewById(R.id.lblGetMoreCoins);
		txtYourCoins 	= (TextView) findViewById(R.id.txtYourCoins);
		btnCoinsPack 	= (Button) findViewById(R.id.btnCoinsPack);
		btnFreeCoins 	= (Button) findViewById(R.id.btnFreeCoins);
		coinsPackLayout = (RelativeLayout) findViewById(R.id.coinsPackLayout);
		freeCoinsLayout = (RelativeLayout) findViewById(R.id.freeCoinsLayout);

		//for ads
		txtAdsText      = (TextView) findViewById(R.id.txtAdsText);
		if(isShowScrennAfterAds){
			txtAdsText.setVisibility(TextView.VISIBLE);
		}

		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}


	/**
	 * This method set text from translation
	 */
	private void setTextFromTranslation()
	{
		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		lblGetMoreCoins.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGetMoreCoins") + "!");

		txtYourCoins.setText( "("+ transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " "
				+ CommonUtils.setNumberString(userCoins + "")  + " " +
				transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")" );

		//for ads
		txtAdsText.setText(transeletion.getTranselationTextByTextIdentifier("lblMakeAppAdsFree"));

		//btnCoinsPack.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		//btnFreeCoins.setText(transeletion.getTranselationTextByTextIdentifier("mfTitleHomeScreen"));
		transeletion.closeConnection();

		if(GET_MORE_COINS_FLAG)
			Log.e(TAG, "outside setTextFromTranslation()");
	}

	/**
	 * This method create dynamic layout for coins packs
	 */
	@SuppressWarnings("deprecation")
	private void createDyanamicLayoutForPricePack(final ArrayList<CoinsPacktransferObj> coinsPackLPriceList)
	{
		btnPriceList = new ArrayList<Button>();

		final LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout child = new RelativeLayout(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for( int i = 0 ; i < coinsPackLPriceList.size() ; i ++ )
		{
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_coins_pack, null);
			layout.addView(child);

			imgCoins 		= (ImageView) child.findViewById(R.id.imgCoins);
			txtPackName   	= (TextView)  child.findViewById(R.id.txtPackName);
			txtCoins        = (TextView)  child.findViewById(R.id.txtCoins);
			btnPrice   		= (Button)    child.findViewById(R.id.btnPrice);

			txtPackName.setText(coinsPackLPriceList.get(i).getName());
			btnPrice.setText("$" + coinsPackLPriceList.get(i).getPrice());
			txtCoins.setText(CommonUtils.setNumberString(coinsPackLPriceList.get(i).getCoins()) + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));

			try
			{
				imgCoins.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.coinsImageFolder) +"/"
						+ coinsPackLPriceList.get(i).getName() + ".png"), null));
			}
			catch (IOException e)
			{
				Log.e(TAG, "createDyanamicLayoutForPricePack : No Image Found");
				//e.printStackTrace();
			}

			btnPriceList.add(btnPrice);

			btnPrice.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					for(int i = 0 ; i < btnPriceList.size() ; i ++ )
					{
						if(v == btnPriceList.get(i))
						{
							//Log.e(TAG,"inside on click " + i);
							clickOnBuy(coinsPackLPriceList.get(i).getName());
						}
					}
				}
			});

			child.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					for(int i = 0 ; i < coinsPackLPriceList.size() ; i ++ )
					{
						if(((RelativeLayout)v) == layout.getChildAt(i))
						{
							//Log.e(TAG,"inside on click " + i);
							clickOnBuy(coinsPackLPriceList.get(i).getName());
						}
					}
				}
			});
		}

		coinsPackLayout.addView(layout);
		transeletion.closeConnection();
	}


	/**
	 *challenger is the product id
	 */
	private void clickOnBuy(String challenger)
	{
		try{
			if(challenger.equals("Amateur Challenger"))
			{
				this.launchPurchseFlow(SKU_AMATEUR_CHALLENGER);
			}
			else if(challenger.equals("Casual Challenger"))
			{
				this.launchPurchseFlow(SKU_CASUAL_CHALLENGER);
			}
			else if(challenger.equals("Serious Challenger"))
			{
				this.launchPurchseFlow(SKU_SIROUS_CHALLENGER);
			}
			else if(challenger.equals("Extreme Challenger"))
			{
				this.launchPurchseFlow(SKU_EXTREME_CHALLENGER);
			}
			else if(challenger.equals("Professional Challenger"))
			{
				this.launchPurchseFlow(SKU_PROFESSIONAL_CHALLENGER);
			}
			else if(challenger.equals("Colossal Challenger"))
			{
				this.launchPurchseFlow(SKU_COLOSSAL_CHALLENGER);
			}else if(challenger.equalsIgnoreCase(SKU_VIDEO_PURCHASED)){
				this.launchPurchseFlow(SKU_VIDEO_PURCHASED);
			}else if(challenger.equalsIgnoreCase(SKU_MATH_APP_CATEGORY_PURCHASED)){
				this.launchPurchseFlow(SKU_MATH_APP_CATEGORY_PURCHASED);
			}
		}catch(Exception e){
			e.printStackTrace();
			MathFriendzyHelper.warningDialog(this, "Problem setting up in-app billing");
		}
	}

	/**
	 * This method start the purchase
	 * @param productId
	 */
	private void launchPurchseFlow(String productId){
		iabPayloadSend = "new_purchase";
		try {
			mHelper.launchPurchaseFlow(GetMoreCoins.this, productId, PURCHASE_REQUEST_CODE,
					mPurchaseFinishedListener, iabPayloadSend);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method create dynamic layout for coins packs
	 */
	private void createDyanamicLayoutForFreePack(final ArrayList<CoinsPacktransferObj> coinsPackLFreeList)
	{
		btnPriceFreeList = new ArrayList<Button>();

		final LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout child = new RelativeLayout(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for( int i = 0 ; i < coinsPackLFreeList.size() ; i ++ )
		{
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_coins_pack, null);
			layout.addView(child);

			imgCoins 		= (ImageView) child.findViewById(R.id.imgCoins);
			txtPackName   	= (TextView)  child.findViewById(R.id.txtPackName);
			txtCoins        = (TextView)  child.findViewById(R.id.txtCoins);
			btnPrice   		= (Button)    child.findViewById(R.id.btnPrice);

			imgCoins.setBackgroundResource(R.drawable.fb);
			txtPackName.setText(coinsPackLFreeList.get(i).getName());
			btnPrice.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo") + "!");
			txtCoins.setText(CommonUtils.setNumberString(coinsPackLFreeList.get(i).getCoins()) + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins"));

			btnPriceFreeList.add(btnPrice);

			btnPrice.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					for(int i = 0 ; i < btnPriceFreeList.size() ; i ++ )
					{
						if(v == btnPriceFreeList.get(i))
						{
							String userid = "";
							String playerId = "";

							TempPlayerOperation tempPlayer = new TempPlayerOperation(GetMoreCoins.this);
							if(!tempPlayer.isTempPlayerDeleted())
							{
								userid   = "0";
								playerId = "0";
							}
							else
							{
								UserPlayerOperation userPlayerOpr = new UserPlayerOperation(GetMoreCoins.this);
								UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(selectedPlayerId);

								userid   = userPlayerData.getParentUserId();
								playerId = selectedPlayerId;
							}

							if(!isAlreadyLike( userid , playerId ))
							{
								userCoins = userCoins + Integer.parseInt(coinsPackLFreeList.get(i).getCoins());
								updateUserFreeCoins(userCoins , userid , playerId);
							}

							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ICommonUtils.FACEBOOK_GETMORE_COINS)));
						}
					}
				}
			});

			child.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					for(int i = 0 ; i < coinsPackLFreeList.size() ; i ++ )
					{
						if(((RelativeLayout)v) == layout.getChildAt(i))
						{
							String userid = "";
							String playerId = "";

							TempPlayerOperation tempPlayer = new TempPlayerOperation(GetMoreCoins.this);
							if(!tempPlayer.isTempPlayerDeleted())
							{
								userid   = "0";
								playerId = "0";
							}
							else
							{

								UserPlayerOperation userPlayerOpr = new UserPlayerOperation(GetMoreCoins.this);
								UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(selectedPlayerId);

								userid   = userPlayerData.getParentUserId();
								playerId = selectedPlayerId;
							}

							if(!isAlreadyLike( userid , playerId ))
							{
								userCoins = userCoins + Integer.parseInt(coinsPackLFreeList.get(i).getCoins());
								updateUserFreeCoins(userCoins , userid , playerId);
							}

							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ICommonUtils.FACEBOOK_GETMORE_COINS)));
						}
					}
				}
			});
		}

		freeCoinsLayout.addView(layout);
		transeletion.closeConnection();
	}

	/**
	 * This method check if user already liked or not
	 */
	private boolean isAlreadyLike(String userId , String playerId)
	{
		InnAppImpl innApp = new InnAppImpl(this);
		innApp.openConn();
		int isLike = innApp.isLiked(userId, playerId);
		innApp.closeConn();

		if(isLike == 1)
			return true;
		else
			return false;
	}

	/**
	 * Update free coins on server
	 * @param userCoins
	 */
	private void updateUserFreeCoins(int userCoins , String userId ,String playerId)
	{
		//set the coins on UI
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtYourCoins.setText( "("+ transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " "
				+ CommonUtils.setNumberString(userCoins + "")  + " " +
				transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")" );
		transeletion.closeConnection();

		InnAppImpl innApp = new InnAppImpl(this);
		innApp.openConn();
		innApp.insertIntoUserFreeCoins(userId, playerId, 1, 0);
		innApp.closeConn();

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();

		if(learningCenterObj.isPlayerTotalPointsExist(playerId))
		{
			learningCenterObj.updateCoinsForPlayer(userCoins, userId, playerId);
		}
		else
		{
			PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
			playerPoints.setUserId(userId);
			playerPoints.setPlayerId(playerId);
			playerPoints.setCoins(userCoins);
			playerPoints.setCompleteLevel(1);
			playerPoints.setPurchaseCoins(0);
			playerPoints.setTotalPoints(0);

			learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);
		}

		learningCenterObj.closeConn();

		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(!tempPlayer.isTempPlayerDeleted())
		{
			tempPlayer = new TempPlayerOperation(this);
			int  tempPlayerId = tempPlayer.getTempPlayerData().get(0).getPlayerId();
			tempPlayer = new TempPlayerOperation(this);
			tempPlayer.updatePlayerCoins(userCoins, tempPlayerId + "");
			tempPlayer.closeConn();
		}
		else
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new SaveCoinsForPlayer(userId, playerId, userCoins).execute(null,null,null);
			}
		}
	}

	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			CommonUtils.printLog(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
			if (result.isFailure()) {
				CommonUtils.printLog(TAG, "Error purchasing: " + result);
				return;
			}

			if (!verifyDeveloperPayload(purchase)) {
				CommonUtils.printLog(TAG, "Error purchasing. Authenticity verification failed.");
				return;
			}

			CommonUtils.printLog(TAG, "Purchase successful.");
			Log.e(TAG,purchase.getSku()+"********");

			//added by me
			Log.e("","**************");
			try {
				mHelper.consumeAsync(purchase, mConsumeFinishedListener);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			CommonUtils.printLog(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

			// We know this is the "gas" sku because it's the only one we consume,
			// so we don't check which sku was consumed. If you have more than one
			// sku, you probably should check...
			if (result.isSuccess()) {
				// successfully consumed, so we apply the effects of the item in our
				// game world's logic, which in our case means filling the gas tank a bit
				CommonUtils.printLog(TAG, "Consumption successful. Provisioning.");

				//added by me
				updateUI(purchase.getSku());

				//Toast.makeText(GetMoreCoins.this,"Consumed succesfully!", Toast.LENGTH_LONG).show();//("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
			}
			else {
				CommonUtils.printLog(TAG, "Error while consuming: " + result);
			}

			CommonUtils.printLog(TAG, "End consumption flow.");

			/*//added by me
            updateUI(purchase.getSku());*/
		}
	};


	/**
	 * This method update UI when coins purchase successfully
	 * challenger is the product id
	 */
	private void updateUI(String challenger)
	{
		if(challenger.equals(SKU_AMATEUR_CHALLENGER))
		{
			this.updateUserpoints("Amateur Challenger");
		}
		else if(challenger.equals(SKU_CASUAL_CHALLENGER))
		{
			this.updateUserpoints("Casual Challenger");
		}
		else if(challenger.equals(SKU_SIROUS_CHALLENGER))
		{
			this.updateUserpoints("Serious Challenger");
		}
		else if(challenger.equals(SKU_EXTREME_CHALLENGER))
		{
			this.updateUserpoints("Extreme Challenger");
		}
		else if(challenger.equals(SKU_PROFESSIONAL_CHALLENGER))
		{
			this.updateUserpoints("Professional Challenger");
		}
		else if(challenger.equals(SKU_COLOSSAL_CHALLENGER))
		{
			this.updateUserpoints("Colossal Challenger");
		}else if(challenger.equalsIgnoreCase(SKU_VIDEO_PURCHASED)){
			this.updateVideoOrFullAppPurchasedStatus(SKU_VIDEO_PURCHASED);
		}else if(challenger.equalsIgnoreCase(SKU_MATH_APP_CATEGORY_PURCHASED)){
			this.updateVideoOrFullAppPurchasedStatus(SKU_MATH_APP_CATEGORY_PURCHASED);
		}
	}

	/**
	 * Update user points
	 * @param challenger
	 */
	private void updateUserpoints(String challenger)
	{
		for( int i = 0 ; i < coinsPackLPriceList.size() ; i ++ )
		{
			if(coinsPackLPriceList.get(i).getName().equals(challenger))
			{
				userCoins = userCoins + Integer.parseInt(coinsPackLPriceList.get(i).getCoins());
				this.updateUserCoins(userCoins , Integer.parseInt(coinsPackLPriceList.get(i).getCoins()));
			}
		}
	}


	/**
	 * This method set the userCoins and update into database
	 * @param userCoins
	 */
	private void updateUserCoins(int userCoins , int coinsFromPack)
	{
		MathFriendzyHelper.saveIsAdDisble(this, 1);

		TempPlayerOperation tempPlayer = new TempPlayerOperation(this);
		if(!tempPlayer.isTempPlayerDeleted())
		{
			//set the coins on UI
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			txtYourCoins.setText( "("+ transeletion.getTranselationTextByTextIdentifier("lblYouHave") + " "
					+ CommonUtils.setNumberString(userCoins + "")  + " " +
					transeletion.getTranselationTextByTextIdentifier("lblCoins") + ")" );
			transeletion.closeConnection();

			tempPlayer = new TempPlayerOperation(this);
			int  tempPlayerId = tempPlayer.getTempPlayerData().get(0).getPlayerId();
			tempPlayer = new TempPlayerOperation(this);
			tempPlayer.updatePlayerCoins(userCoins, tempPlayerId + "");
			tempPlayer.closeConn();

			/*LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
			learningCenterObj.openConn();
			learningCenterObj.updateCoinsForPlayer(userCoins, "0", "0");				
			learningCenterObj.closeConn();*/

			LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
			learningCenterObj.openConn();

			String userid   = "0";
			String playerId = "0";

			if(learningCenterObj.isPlayerTotalPointsExist(playerId))
			{
				learningCenterObj.updateCoinsForPlayer(userCoins, userid, playerId);
			}
			else
			{
				PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
				playerPoints.setUserId(userid);
				playerPoints.setPlayerId(playerId);
				playerPoints.setCoins(userCoins);
				playerPoints.setCompleteLevel(1);
				playerPoints.setPurchaseCoins(0);
				playerPoints.setTotalPoints(0);

				learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);
			}

			learningCenterObj.closeConn();
		}
		else
		{
			if(regUserObj.getCoins().length() > 0 )
				userCoins = coinsFromPack + Integer.parseInt(regUserObj.getCoins());
			else
				userCoins = coinsFromPack + 0;

			UserRegistrationOperation userRegOpr = new UserRegistrationOperation(this);
			userRegOpr.updaUserCoins(userCoins, regUserObj.getUserId());
			userRegOpr.closeConn();

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				//for ads
				SharedPreferences sharedPreff = getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
				SharedPreferences.Editor editor = sharedPreff.edit();
				editor.putInt(ICommonUtils.ADS_IS_ADS_DISABLE, 1);
				editor.commit();
				//end changes

				new SaveCoinsForUser(regUserObj.getUserId(), userCoins).execute(null,null,null);
				new UpdateAdsPurchaseStatus(regUserObj.getUserId()).execute(null,null,null);
			}
		}
	}


	/** Verifies the developer payload of a purchase. */
	boolean verifyDeveloperPayload(Purchase p) {
		String payload = p.getDeveloperPayload();
		if(payload.equals(iabPayloadSend))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetCoinsPack extends AsyncTask<Void, Void, Void>
	{
		private ArrayList<CoinsPacktransferObj> coinsPackList = null;
		ProgressDialog pd = null;

		@Override
		protected void onPreExecute()
		{
			pd = CommonUtils.getProgressDialog(GetMoreCoins.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			InAppServerOperation serverObj = new InAppServerOperation();
			coinsPackList = serverObj.getCoinsPack();
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			pd.cancel();

			if(coinsPackList != null){
				coinsPackLPriceList = new ArrayList<CoinsPacktransferObj>();
				coinsPackLFreeList  = new ArrayList<CoinsPacktransferObj>();

				for(int i = 0 ; i < coinsPackList.size() ; i ++ )
				{
					if(coinsPackList.get(i).getType().equals("Free"))
					{
						coinsPackLFreeList.add(coinsPackList.get(i));
					}
					else
					{
						coinsPackLPriceList.add(coinsPackList.get(i));
					}
				}

				createDyanamicLayoutForPricePack(coinsPackLPriceList);
				createDyanamicLayoutForFreePack(coinsPackLFreeList);
			}else{
				CommonUtils.showInternetDialog(GetMoreCoins.this);
			}
			super.onPostExecute(result);
		}
	}


	/**
	 * This class update coins on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveCoinsForUser extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private int coins;
		ProgressDialog pd;

		SaveCoinsForUser(String userId , int coins)
		{
			this.userId = userId;
			this.coins  = coins;
		}

		@Override
		protected void onPreExecute()
		{
			pd = CommonUtils.getProgressDialog(GetMoreCoins.this);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			InAppServerOperation serverObj = new InAppServerOperation();
			serverObj.saveCoinsForUser(userId, coins);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			pd.cancel();
			startActivity(new Intent(GetMoreCoins.this , CoinsDistributionActivity.class));
			super.onPostExecute(result);
		}
	}

	/**
	 * This class update coins on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveCoinsForPlayer extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private int coins;
		private String playerId;
		//ProgressDialog pd;

		SaveCoinsForPlayer(String userId , String playerId , int coins)
		{
			this.userId = userId;
			this.coins  = coins;
			this.playerId = playerId;
		}

		@Override
		protected void onPreExecute()
		{
			/*pd = CommonUtils.getProgressDialog(GetMoreCoins.this);
			pd.show();*/
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			InAppServerOperation serverObj = new InAppServerOperation();
			serverObj.saveCoinsForPlayer(userId, playerId, coins);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			//pd.cancel();
			super.onPostExecute(result);
		}
	}

	/**
	 * This class update the ads purchase status
	 * @author Yashwant Singh
	 *
	 */
	class UpdateAdsPurchaseStatus extends AsyncTask<Void, Void, Void>{

		private String userId = null;
		UpdateAdsPurchaseStatus(String userId){
			this.userId = userId;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			AdsServerOperation adsServerOperation = new AdsServerOperation();
			adsServerOperation.updateAdsPurchaseStatus(userId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}

	private void startPurchased(){
		if(appRequest != DEFAULT_PURCHASED_REQUEST) {
			final ProgressDialog pd = MathFriendzyHelper.getProgressDialog(this , "");
			MathFriendzyHelper.showProgressDialog(pd);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					MathFriendzyHelper.hideProgressDialog(pd);
					if (appRequest == VIDEO_PURCHASED) {
						clickOnBuy(SKU_VIDEO_PURCHASED);
					}else if(appRequest == UNLOCK_MATH_APP_CATEGORY_PURCHASED){
						clickOnBuy(SKU_MATH_APP_CATEGORY_PURCHASED);
					}
				}
			}, 1000);
		}
	}

	private void updateVideoOrFullAppPurchasedStatus(String challengerId){
		if(challengerId.equalsIgnoreCase(SKU_VIDEO_PURCHASED)){
			if(!MathFriendzyHelper.isUserLogin(this)){
				MathFriendzyHelper.saveBooleanInSharedPreff(this ,
						MathFriendzyHelper.RESOURCE_PURCHASE_STATUS_KEY_FOR_DEVICE , true);
				finishActivityWhenPurchaseSuccess();
				return ;
			}

			MathFriendzyHelper.saveVideoInAppStatus(this ,
					MathFriendzyHelper.getUserId(this) , new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase,
						int requestCode) {

				}
			});
		}else if(challengerId.equalsIgnoreCase(SKU_MATH_APP_CATEGORY_PURCHASED)){
			if(!MathFriendzyHelper.isUserLogin(this)){
				MathFriendzyHelper.saveBooleanInSharedPreff(this ,
						MathFriendzyHelper.UNLOCK_CATEGORY_PURCHASE_STATUS_KEY_FOR_DEVICE , true);
				finishActivityWhenPurchaseSuccess();
				return ;
			}

			MathFriendzyHelper.updateCategoryPurchasedStatus(this ,
					MathFriendzyHelper.getUserId(this) , new HttpResponseInterface() {
				@Override
				public void serverResponse(HttpResponseBase httpResponseBase,
						int requestCode) {

				}
			} , MathFriendzyHelper.NO);
		}
		finishActivityWhenPurchaseSuccess();
	}

	private void finishActivityWhenPurchaseSuccess(){
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		finish();
	}
}
