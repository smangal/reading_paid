package com.mathfriendzy.controller.school;

import static com.mathfriendzy.utils.ICommonUtils.SCHOOL_INFO_SUB_FLAG;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.controller.player.AddPlayer;
import com.mathfriendzy.controller.player.AddTempPlayerStep2Activity;
import com.mathfriendzy.controller.player.EditPlayer;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.controller.registration.RegisterationStep2;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schools.Schools;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.readingfriendzypaid.R;

/**
 * This Activity is submit the school information to the Server when the user can not find your school in the given 
 * school detail
 * @author Yashwant Singh
 *
 */
public class SchoolInfoSubmitter extends AdBaseActivity implements OnClickListener
{
	private Spinner spinnerCountry = null;
	private Spinner spinnerState   = null;
	private EditText edtSchoolName = null;
	private EditText edtCity       = null;
	private EditText edtZipCode    = null;
	private Button   btnSavePlayer = null;
	
	private final String TAG = this.getClass().getSimpleName();
	private ArrayList<String>     countryNameList = null;
	
	public static String countryName = null;
	public static String stateName   = null;
	public static String city        = null;
	public static String zip         = null;
	
	private TextView mfTitleHomeScreen 		= null;
	private TextView schoolInfoPlaceholder 	= null;
	private TextView lblSchoolInfoInfo  	= null;
	private TextView lblRegSchool 			= null;
	private TextView lblRegCountry 			= null;
	private TextView lblRegCity 			= null;
	private TextView lblRegZip 				= null;
	private TextView lblRegState   			= null;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_school_info_submitter);
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside onCreate()");
		
		EditPlayer.IS_REGISTER_SCHOOL 				  = true;
		AddTempPlayerStep2Activity.IS_REGISTER_SCHOOL = true;
		EditRegisteredUserPlayer.IS_REGISTER_SCHOOL   = true;
		AddPlayer.IS_REGISTER_SCHOOL      			  = true;
		RegisterationStep2.IS_REGISTER_SCHOOL 		  = true;
		
		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getCountries();
		this.setWidgetsValues();
		this.setListenerOnWidgets();
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outsideside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
		spinnerState   = (Spinner) findViewById(R.id.spinnerState);
		edtSchoolName  = (EditText)findViewById(R.id.edtSchoolName);
		edtCity        = (EditText)findViewById(R.id.edtCity);
		edtZipCode     = (EditText)findViewById(R.id.edtZipCode);
		btnSavePlayer  = (Button)  findViewById(R.id.btnTitleSave);
		
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		schoolInfoPlaceholder 	= (TextView) findViewById(R.id.schoolInfoPlaceholder);
		lblSchoolInfoInfo 		= (TextView) findViewById(R.id.lblSchoolInfoInfo);
		lblRegSchool 			= (TextView) findViewById(R.id.lblRegSchool);
		lblRegCountry 			= (TextView) findViewById(R.id.lblRegCountry);
		lblRegCity 				= (TextView) findViewById(R.id.lblRegCity);
		lblRegZip 				= (TextView) findViewById(R.id.lblRegZip);
		lblRegState    			= (TextView)findViewById(R.id.lblRegState);
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * this method set the widgets text values from the translation
	 */
	private void setWidgetsTextValues() 
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setWidgetsTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy"));
		schoolInfoPlaceholder.setText(transeletion.getTranselationTextByTextIdentifier("schoolInfoPlaceholder" + ": "));
		lblSchoolInfoInfo.setText(transeletion.getTranselationTextByTextIdentifier("lblSchoolInfoInfo"));
		lblRegSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblRegSchool") + ": " );
		lblRegCountry.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry") + ": ");
		lblRegState.setText(transeletion.getTranselationTextByTextIdentifier("lblRegState") + ": ");
		lblRegCity.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCity") + ": " );
		lblRegZip.setText(transeletion.getTranselationTextByTextIdentifier("lblRegZip") + ": " );
		btnSavePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSave"));
		transeletion.closeConnection();
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside setWidgetsTextValues()");
	}
	
	/**
	 * This method get Countries from the database
	 */
	private void getCountries()
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside getCountries()");
		
		Country countryObj = new Country();
		countryNameList = countryObj.getCountryName(this);
			
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside getCountries()");
	}
	
	/**
	 * This method set the widgets values from based on the user information
	 * like as city , country , and zip
	 */
	private void setWidgetsValues() 
	{	
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setWidgetsValues()");
		
		this.setCountryAdapter();
		edtCity.setText(city);
		edtZipCode.setText(zip);
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside setWidgetsValues()");
	}
	
	/**
	 * this method set country to the spinner 
	 */
	private void setCountryAdapter()
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setCountryAdapter()");
		
		if(countryNameList != null)
		{
			ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(SchoolInfoSubmitter.this, android.R.layout.simple_list_item_1,countryNameList);
			countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerCountry.setAdapter(countryAdapter);
			spinnerCountry.setSelection(countryAdapter.getPosition(countryName));
		}
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside setCountryAdapter()");
	}
	
	/**
	 * This methos set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setListenetOnWidgets()");
		
		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3) 
			{				
				if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					lblRegZip.setTextColor(Color.BLACK);
					edtZipCode.setEnabled(true);
					setStates("United States");
				}
				else if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				{
					edtZipCode.setEnabled(true);
					lblRegZip.setTextColor(Color.BLACK);
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					setStates("Canada");
				}
				else
				{
					edtZipCode.setEnabled(false);
					lblRegZip.setTextColor(Color.GRAY);
					spinnerState.setVisibility(Spinner.GONE);
					lblRegState.setVisibility(TextView.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{				
			}
		});
		
		btnSavePlayer.setOnClickListener(this);
	}
	
	/**
	 * This method set states when user select country as United States and Canada
	 * @param countryName
	 */
	private void setStates(String countryName)
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside setStates()");
		
		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(countryName.equals("United States"))
			satateList = stateObj.getUSStates(this);
		else if(countryName.equals("Canada"))
			satateList = stateObj.getCanadaStates(this);
		
		if(satateList != null)
		{
			ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(SchoolInfoSubmitter.this, android.R.layout.simple_list_item_1,satateList);
			stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerState.setAdapter(stateAdapter);
			spinnerState.setSelection(stateAdapter.getPosition(stateName));
			stateAdapter.notifyDataSetChanged();
		}
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside setStates()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
			case R.id.btnTitleSave : 
				this.validateField();
				break;
		}
	}
	
	/**
	 * This method check for empty validation before submitting information on server
	 */
	private void validateField()
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside validateField()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		
		if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			if(edtSchoolName.getText().toString().equals("") || edtCity.getText().toString().equals("")
					||edtZipCode.getText().toString().equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.registerSchool();
			}
		}
		else
		{
			if(edtSchoolName.getText().toString().equals("") || edtCity.getText().toString().equals(""))
			{
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.registerSchool();
			}
		}
		transeletion.closeConnection();
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside validateField()");
	}
	
	/**
	 * This method registered school on server
	 */
	private void registerSchool()
	{
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "inside registerSchool()");
		
		Country countryObj = new Country();
		String countryISO = countryObj.getCountryIsoByCountryName(countryName, this);
		String countryId  = countryObj.getCountryIdByCountryName(countryName, this);
		
		//changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new RegisetereSchoolAsyncTask(edtSchoolName.getText().toString(),countryISO,countryId,edtCity.getText().toString()).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(SCHOOL_INFO_SUB_FLAG)
			Log.e(TAG, "outside registerSchool()");
	}
	
	/**
	 * AsyncTask for Registering School on server
	 * @author Yashwant Singh
	 *
	 */
	class RegisetereSchoolAsyncTask extends AsyncTask<Void, Void, Void>
	{
		String city 		= null;
		String countryIso 	= null;
		String countryId 	= null;
		String schoolName 	= null;
		String schoolId     = null;
		
		RegisetereSchoolAsyncTask(String schoolName,String countryIso,String countryId,String city)
		{
			this.city = city;
			this.countryId = countryId;
			this.countryIso = countryIso;
			this.schoolName = schoolName;
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			if(SCHOOL_INFO_SUB_FLAG)
				Log.e(TAG, "inside RegisetereSchoolAsyncTask doInBackground()");
			
			Schools schoolObj = new Schools();
			schoolId = schoolObj.registereSchool(schoolName, countryId, countryIso, city);
						
			if(SCHOOL_INFO_SUB_FLAG)
				Log.e(TAG, "outside RegisetereSchoolAsyncTask doInBackground()");
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			ArrayList<String> schoolIdAndName = new ArrayList<String>();
			schoolIdAndName.add(schoolName);
			schoolIdAndName.add(schoolId);
			
			Intent returnIntent = new Intent();
			returnIntent.putExtra("schoolInfo",schoolIdAndName);
			setResult(RESULT_OK,returnIntent);     
			finish();
			super.onPostExecute(result);
		}
	}
}
