package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.readingfriendzypaid.R;


public class ChooseChallengerAdapter extends BaseAdapter 
{
	private LayoutInflater mInflater   = null;
	private ViewHolder vholder 		   = null;
	private Context context 		   = null;
	private int resource 			   = 0;
	private int completeLevel          = 0;
	
	private ArrayList<ChallengerTransferObj> challengerList = null;
	
	private ArrayList<TextView> txtPointsList 		= null;
	private ArrayList<RelativeLayout> layoutList 	= null;
	private ArrayList<TextView> txtLevelList        = null;
	
	public static ChallengerTransferObj challengerDataObj = null;// to transfer value to the next screen
	
	ChooseChallengerAdapter(Context context ,  int resource , ArrayList<ChallengerTransferObj> challengerList )
	{
		this.resource 			= resource;
		mInflater 				= LayoutInflater.from(context);
		this.context 			= context;
		this.challengerList   	= challengerList;
		
		txtPointsList 	= new ArrayList<TextView>();
		layoutList 		= new ArrayList<RelativeLayout>();
		txtLevelList    = new ArrayList<TextView>();
		
		for(int i = 0 ; i < challengerList.size() ; i ++ )
		{
			txtPointsList.add(null);
			layoutList.add(null);
			txtLevelList.add(null);
		}
		
		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		
		completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);//get played complete level
	}
	
	@Override
	public int getCount() 
	{
		return challengerList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return null;
	}

	@Override
	public long getItemId(int position) 
	{
		
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup parent) 
	{
		if(view == null)
		{
			vholder 			   	= new ViewHolder();
			view 				  	= mInflater.inflate(resource, null);
			vholder.imgFlag         = (ImageView)  view.findViewById(R.id.imgFlag);
			vholder.txtName   		= (TextView)   view.findViewById(R.id.txtName);
			vholder.txtPoints   	= (TextView)   view.findViewById(R.id.txtpoints);
			vholder.txtLevel		= (TextView)   view.findViewById(R.id.txtLevel);
			vholder.problemLayout   = (RelativeLayout) view.findViewById(R.id.reletiveAttributes);
					
			view.setTag(vholder);
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
		
		txtPointsList.set(index, vholder.txtPoints);
		layoutList.set(index, vholder.problemLayout);
		txtLevelList.set(index, vholder.txtLevel);
		
		this.setCountryImage(index, vholder.imgFlag);
		vholder.txtName.setText(MathFriendzyHelper.getStudentName(challengerList.get(index).getFirst() ,
                challengerList.get(index).getLast()));
//		vholder.txtName.setText(challengerList.get(index).getFirst() + " " + challengerList.get(index).getLast().charAt(0)+".");
		vholder.txtPoints.setText(CommonUtils.setNumberString(challengerList.get(index).getSum()));
		vholder.txtLevel.setText(completeLevel + "");
		
		vholder.problemLayout.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				layoutList.get(index).setBackgroundResource(R.drawable.mcg_green_bar);
				Country country = new Country();
				
				txtPointsList.get(index).setText(country
						.getCountryNameByCountryIso(challengerList.get(index).getCountryIso(), context));
				
				txtLevelList.get(index).setText("");
				
				callForNextActivity(challengerList.get(index));
			}
		});
		
		return view;
	}

	
	/**
	 * This method set countryImage
	 */
	@SuppressWarnings("deprecation")
	private void setCountryImage(int index , ImageView imgFlag)
	{
		try 
		{
			
			imgFlag.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open(context.getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ challengerList.get(index).getCountryIso() + ".png"), null));
		} 
		catch (IOException e) 
		{
			Country country = new Country();
			try 
			{
				imgFlag.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open(context.getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ country.getCountryIsoByCountryId(challengerList.get(index).getCountryIso(), context) + ".png"), null));
			}
			catch (NotFoundException e1) 
			{
				Log.e("ChooseChallengerAdapter", "Inside setCountryImage Error No Image Found");
				//e1.printStackTrace();
			} 
			catch (IOException e1) 
			{
				Log.e("ChooseChallengerAdapter", "Inside setCountryImage Error No Image Found");
				//e1.printStackTrace();
			}
			
			//e.printStackTrace();
		}
	}
	
	
	/**
	 * This method when user click on challenger player
	 * @param challengerDataObj
	 */
	private void callForNextActivity(ChallengerTransferObj challengerDataObj)
	{
		this.challengerDataObj = challengerDataObj;
		context.startActivity(new Intent(context , SingleFriendzyEquationActivity.class));
	}
	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		ImageView   imgFlag;
		TextView    txtName ;
		TextView    txtPoints;
		TextView    txtLevel;
		RelativeLayout problemLayout;
	}
}
