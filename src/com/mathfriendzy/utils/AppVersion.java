package com.mathfriendzy.utils;

public interface AppVersion {
	int SIMPLE_VERSION = 1;
	int PAID_VERSION = 2;
	int CURRENT_VERSION = PAID_VERSION;
}
